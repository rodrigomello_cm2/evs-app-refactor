var timeSyncInitial = false;
var timeSyncTimer   = false;

var debugFunc       = true;

if(debugTipo == "browser" || debugTipo == "both"){
    debugFunc = console.log;
    
    console.log = function(mensagem) {
        $("#page-debug").prepend("<p>" + mensagem + "</p>");
        if(debugTipo === "both") debugFunc(mensagem);
    }

    $(document).on("taphold", "#loading, .form-signin-heading, .navbar-text", function(e){
        e.preventDefault();
        e.stopPropagation();
        
        if(window.pageActual == "page-debug"){
            pagesClass.toPage("page-login");
        } else {
            pagesClass.toPage("page-debug");
        }
    });
}

window.onerror = function(message, source, lineno, colno, error){
    if(debugDev) console.log("js erro " + message + " \nsrc:" + source + " " + lineno);
}



var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
        
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        if(typeof cordova === "undefined"){
            app.onDeviceReady();
        } else {
            document.addEventListener('deviceready', this.onDeviceReady, false);
            document.addEventListener('backbutton', this.backButton, false);
        }
    },
    
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        
        FastClick.attach(document.body);
        
        if(typeof device != "undefined") $('body').addClass(device.platform);
        
        // inicializa timer de loading        
        setInterval(function(){
            if(window.syncLock && !window.syncHide){
                $('#loading').show();
            } else {
                $('#loading').hide();
            }
        }, 500);
        
        // inicializa db caso nao tenha sido ainda
        if(!db) initDB();
        
        // verifica a estrutura basica das tabelas
        syncClass.initialCheck();
        
        // inicia um interval para iniciar sync unico assim que estiver logado
        timeSyncInitial = setInterval(function(){ 
            if(window.isLogged){
                syncClass.processPopulateTables();
                clearInterval(timeSyncInitial);
                timeSyncInitial = false;
            }
        }, 500);
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        if(debugDev) console.log('Received Event: ' + id);
    },

    backButton: function( e ){
        pagesClass.goBack();
    }
};

app.initialize();
