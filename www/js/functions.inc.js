if(debugTipo === "browser" || debugTipo === "both"){
    console.logOld = console.log;
    console.log = function(mensagem) {
        $(".debug_area").prepend("<li>"+mensagem);
        if(debugTipo === "both") console.logOld(mensagem);
    };

    //$(document).on("taphold", function(){
    //    $.mobile.changePage("#page-debug", { transition: 'slide' });
    //});
}

window.onerror = function(message, source, lineno, colno, error){
    if(debugDev) console.log("JS error: " + message + " \nSrc:" + source + " -> " + lineno);
}

isOnline = function () {
    var ret = false;
    
    try {
        if(typeof navigator.connection === 'undefined'){
            if(typeof device === "undefined"){
                ret = true;
            } else {
                ret = false;
            }
        } else {
            var axConexao = navigator.connection.type.toString().toLowerCase();
            ret = !(axConexao === 'none');
        }
    } catch(e){
        ret = false;
        if(debugDev) console.log("Erro ao verificar conexao: " + e);
    }

    return ret;
};

getLocalDate = function(){
    var dt = new Date();
    var minutes = dt.getTimezoneOffset() * -1;
    dt = new Date(dt.getTime() + minutes*60000);
    
    return dt;
}
  
now = function(){
    return formataData(getLocalDate());
}

cropData = function(data, pos){
    if(typeof pos == "undefined" || !pos) pos = 3;
    
    if(pos == 3) return data.substring(8, 10) + "/" + data.substring(5, 7) + "/" + data.substring(0, 4);
    if(pos == 2) return data.substring(8, 10) + "/" + data.substring(5, 7);
    if(pos == 1) return data.substring(8, 10);
}

formataData = function(data){
    return data.toJSON().replace("T", " ").substring(0, 19);
}

formataPreco = function(valor){
    if( typeof valor !== 'number' ) {
        valor = 0;
    }

    valor = valor.toFixed(2);
    valor = valor.replace('.', ',');
    
    var tmp = valor.split(',');
    
    var c = 0;
    var ini = "";
    
    for(x=(tmp[0].length - 1); x>=0; x--){
        c++;
        
        ini = tmp[0][x] + ini;
        if(c == 3 && x > 0){
            ini = "." + ini;
            c = 0;
        }
    }
    
    valor = ini + "," + tmp[1];
    
    return valor;
}

cleanImgData = function(img){
    img = img.replace('url("', '');
    img = img.replace('")', '');

    img = img.replace('url(', '');
    img = img.replace(')', '');
    
    return img;
}

zfill = function(num, len) {
    return (Array(len).join("0") + num).slice(-len);
}

guid = function() {
    s4 = function() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    var data = new Date();

    return data.getYear() + zfill(data.getMonth(), 2) + zfill(data.getDay(), 2) + zfill(data.getHours(), 2) + zfill(data.getMinutes(), 2) + zfill(data.getSeconds(), 2) + "-" + zfill(localStorage.getItem('id_anfitriao'), 5) + "-" + s4() + '-' + s4() + s4() + s4();
}

$(document).on('keypress', '.numerico, .mask-integer, .mask-int', function(e){
    if( e.which != 46 && e.which != 44 && e.which != 13 && e.which != 8 && e.which != 0 && ( e.which < 48 || e.which > 57 ) ){
        e.stopPropagation();
        e.preventDefault();
        return false;
    }
});

$('.mask-phone').mask('(00) 00000-0000');
$('.mask-date').mask('00/00/0000');