/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var auxExportData    = {};
var auxExportQueue   = [];
var auxExportControl = [];

window.syncLock = false;
window.syncHide = false;
window.syncType = "basic";

var syncClass = {
    initialCheck: function(){
        syncClass.checkBasicStructureCreated();
    },
    
    checkBasicStructureCreated: function(){
        if(window.syncLock) return false;
        window.syncLock = true;
        window.syncType = "basic";
        
        execSQL("SELECT 1 FROM anfitriao LIMIT 1", null, syncClass.proceedAfterBasicCheck, function(t, e){
            var msg = e.message;
            
            if(msg.indexOf("no such table") > -1){
                syncClass.createBasicStructure(syncClass.proceedAfterBasicCheck);
            } else {
                window.syncLock = false;
                
                if(debugDev) console.log("Erro desconhecido ao verificar estrutura basica");
                return false;
            }
        });
    },
    
    createBasicStructure: function(callback){
        $.get("db/app-db.txt", function (d) {
            if(debugDev) console.log("Criando estrutura basica");
            
            var lins = d.split(";");
            var cmds = new Array();

            for(var i in lins){
                var lin = lins[i];

                lin = lin.replace(/[\n\r]/g, ' ');
                cmds.push(lin);
            }
            
            cmds.push("INSERT INTO controle SELECT tbl_name, '0' AS atualizar, '2016-01-01 00:00:00' AS dta FROM sqlite_master WHERE type = 'table' AND tbl_name <> '__WebKitDatabaseInfoTable__' AND tbl_name <> 'controle' ORDER BY tbl_name ASC");

            execSQLBatch(0, cmds, callback);
        });
    },
    
    resetBasicStructure: function(callback){
        $.get("db/drop-db.txt", function (d) {
            if(debugDev) console.log("Resetando estrutura basica");
            
            var lins = d.split(";");
            var cmds = new Array();

            for(var i in lins){
                var lin = lins[i];

                lin = lin.replace(/[\n\r]/g, ' ');
                cmds.push(lin);
            }

            n = cmds.length;
            
            execSQLBatch(0, cmds, callback);
        });
    },
    
    proceedAfterBasicCheck: function(){
        window.syncLock = false;
        
        if(debugDev) console.log("Verificacao de estrutura basica finalizada");
        
        pagesClass.toPage("page-login");
    },
    
    processPopulateTables: function(){
        if(window.syncLock || !window.isLogged) return false;
        window.syncLock = true;
        window.syncType = "populate";
        
        if(debugDev) console.log("Iniciando sync de dados");

        execSQL("\
            SELECT  tabela, dta \
            FROM    controle \
            ORDER BY    tabela ASC", null, function(ts, res){

            auxExportControl = [];
            if(res.rows.length) for(var i = 0; i < res.rows.length; i++) auxExportControl.push(res.rows.item(i));
            
            if(debugData) console.log("Tabela controle: ");
            if(debugData) console.log(auxExportControl);
            
            syncClass.exportTableRows();
        });
    }, 
    
    
    insertTableRows: function(tabelas){
        var sql = [];
        
        $.each(tabelas, function(tabela, linhas){
            if(linhas.length){
                sql = sql.concat(syncClass.prepareInsertRow(tabela, linhas));
                sql.push("UPDATE controle SET dta = (SELECT dta FROM " + tabela + " ORDER BY dta DESC LIMIT 1) WHERE tabela = '" + tabela + "'");
            }
        });
       
        execSQLBatch(0, sql, function(){
            if(debugDev) console.log("Finalizando a insercao dos registros na base");
            
            // finaliza sincronismo
            window.syncLock = false;
            window.syncHide = true;

            // dispara o sync temporal
            if(!timeSyncTimer){
                timeSyncTimer = setInterval(function(){ 
                    if(debugDev) console.log("Sync temporal iniciado");
                    authClass.checkValidLogin();
                    syncClass.processPopulateTables(); 
                }, 20000);
            }
        });
    },
    
    prepareInsertRow: function(tabela, linhas){
        var saida  = [];
        var campos = [];
        var places = [];
        
        $.each(linhas, function(idx, linha){
            var values = [];
            
            $.each(linha, function(campo, valor){
                if(idx === 0){
                    campos.push(campo);
                    places.push('?');
                }
                
                values.push(valor);
            });
            
            var sql = 'INSERT OR REPLACE INTO ' + tabela + '(' + campos.join(', ') + ') VALUES (' + places.join(', ') + ')';

            saida.push({ 'sql': sql, 'values': values });
        });
        
        return saida;
    },
    
    exportTableRows: function(){
        if(debugDev) console.log("Iniciando export dos dados");
        
        window.syncType = "export";
        
        execSQL("\
            SELECT  tabela \
            FROM    controle \
            WHERE   atualizar = 1 \
            ORDER BY    tabela ASC", null, function(ts, res){
            
            auxExportData  = {};
            auxExportQueue = [];
            
            if(res.rows.length) for(var i = 0; i < res.rows.length; i++) auxExportQueue.push(res.rows.item(i).tabela);
            
            syncClass.loopExportRows();
        });
    }, 
    
    loopExportRows: function(){
        tabela = auxExportQueue.splice(0, 1);
        
        if(tabela.length){
            tabela = tabela[0];
            
            execSQL("\
                SELECT  * \
                FROM    " + tabela + " \
                WHERE   dta IS NULL \n\
                ORDER BY    " + (tabela == "venda_produto" ? "id_parent" : "dta"), null, function(ts, res){

                auxExportData[tabela] = [];
                
                if(res.rows.length) for(var i = 0; i < res.rows.length; i++) auxExportData[tabela].push(res.rows.item(i));

                execSQL("UPDATE controle SET atualizar = ? WHERE tabela = ?", [ 0, tabela ], function(){
                    execSQL("UPDATE " + tabela + " SET dta = ? WHERE dta IS NULL", [ '2016-01-01 00:00:00' ], function(){
                        syncClass.loopExportRows();
                    });
                });
            });
        } else {
            if(debugDev)  console.log("Dados selecionados para exportar: ");
            if(debugData) console.log(auxExportData);
            
            syncClass.sendExportedRows();
        }
    },
    
    sendExportedRows: function(){
        $.ajax({
            url: urlRemote + "/updateReceive",
            beforeSend: function(xhr) { xhr.setRequestHeader('Authorization', 'Basic ' + btoa(localStorage.getItem('login') + ':' + localStorage.getItem('senha'))); },
            data: { 'controle': auxExportControl, 'tables': auxExportData },
            method: 'POST',
            datatype: 'json',

            success: function (data) {
                // deu bom, termina o sync      
                if(debugDev)  console.log("Exportação encerrada");
            },

            statusCode: {
                401: function() {
                    // deu ruim por algum motivo
                    if(debugDev) console.log("Houve um problema durante o envio dos dados exportados");

                    window.syncLock = false;
                },

                500: function(a, b) {
                    // deu MUITO ruim por algum motivo
                    if(debugDev) console.log("Houve um problema durante o envio dos dados exportados");
                    if(debugDev) console.log(JSON.stringify(a));
                    if(debugDev) console.log(JSON.stringify(b));
                    
                    clearInterval(timeSyncTimer);
                    timeSyncTimer = false;
                    
                    return false;
                },

                200: function(data) {
                    data = $.parseJSON(data);
                    
                    if(debugDev)  console.log("Retorno da exportação recebido");
                    if(debugData) console.log(data["import"]);

                    if(debugDev)  console.log("Dados para importação recebidos");
                    if(debugData) console.log(data["export"]);
                    
                    syncClass.insertTableRows(data["export"]);
                }
            }                    
        });     
    }
};

