$(document).on('click', '#loading', function(e){
    e.preventDefault();
    e.stopPropagation();
});

$(document).on('submit', '#page-login form', function(e){
   e.preventDefault();
   
   $('#page-login .alert').hide();
   
   var login = $('#page-login #inputLogin').val();
   var senha = $('#page-login #inputPassword').val();
   
   if(login.length < 3 || senha.length < 3){
       $('#page-login .alert').show('fast');
       return false;
   }
   
   authClass.processLogin(login, senha);
});

$(document).on('click', '#btn-menu', function(e){
    e.preventDefault();
    e.stopPropagation();
    $('#menu').toggle();
});

$(document).on('click', '#menu a', function(e){
    e.preventDefault();
    e.stopPropagation();
    
    var acao = $(this).attr('href').toString().substr(1);
    
    if(debugDev) console.log("Acao de menu selecionada: " + acao);
    
    $('#menu').toggle();
    
    $('.alert').hide();
    
    switch(acao){
        case 'vendas':
            carrinhoClass.renderVendasAbertas();
        break;
        
        case 'produtos':
            pagesClass.toPage("page-produtos");
        break;
        
        case 'clientes':
            $('#page-clientes-lista .alert').hide();
            pagesClass.toPage("page-clientes-lista");
        break;
        
        case 'relatorio-venda-diario':
        case 'relatorio-atividade-diario':
        case 'relatorio-mensal-ganho':
        case 'relatorio-anfitrioes':
                pagesClass.preparePage(acao);
        break;
		
        case 'sync':
            if(debugDev) console.log("Iniciando manualmente o sync");
            syncClass.processPopulateTables(); 
        break;
        
        case 'logoff':
            pagesClass.pageHistory = [];
            authClass.processLogoff();
        break;
    }
    
});



$(document).on('click', '.controles #btn-lista-vendas', function(e){
   e.preventDefault();
   
   carrinhoClass.renderVendasAbertas();
});

$(document).on('click', '.controles #btn-go-carrinho', function(e){
   e.preventDefault();
   
   carrinhoClass.beginRenderCarrinhoFinal(false);
});

$(document).on('click', '.controles #btn-back-carrinho', function(e){
   e.preventDefault();
   
   $('.alert').hide();
   pagesClass.toPage('page-carrinho');
});

$(document).on('click', '.controles #btn-back-produtos', function(e){
   e.preventDefault();
   
   $('.alert').hide();
   carrinhoClass.beginRenderCarrinho(function(){ pagesClass.toPage('page-produtos'); }, false);
});


$(document).on('click', '.controles #btn-add-cliente', function(e){
   e.preventDefault();
   
   localStorage.removeItem('id_cliente');
   
   $('.alert').hide();
   clientesClass.beginClientAdd(false);
});

$(document).on('click', '.controles #btn-back-cliente', function(e){
   e.preventDefault();
   
   $('.alert').hide();
   pagesClass.toPage('page-clientes-lista');
});

$(document).on('click', '.take-photo', function(e){
    e.preventDefault();

    clientesClass.cameraTakePhoto($(this));
});


$(document).on('submit', '#page-clientes-lista .busca', function(e){
   e.preventDefault();
   
   var txt = $(this).find('#inputSearch').val();
   
   clientesClass.processClientsList(false, false, txt);
});


$(document).on('submit', '#page-clientes-incluir .form-clientes-incluir', function(e){
    e.preventDefault();
    
    $('#page-clientes-incluir .alert').hide();
    
    var passou = true,
        cfg = {},
        msgs = [];
    
    cfg["nome"]                 = $(this).find('#inputNome').val();
    cfg["email"]                = $(this).find('#inputEmail').val();
    cfg["foto"]                 = $(this).find('.media img').attr('src');
    cfg["id_anfitriao"]         = $(this).find('#inputAnfitriao').val();
    cfg["id_tipo_indicacao"]    = $(this).find('#inputTipoIndicacao').val();
    cfg["telefone_celular"]     = $(this).find('#inputTelefone').val();
    cfg["data_nascimento"]      = $(this).find('#inputDataNascimento').val();
    cfg["senha"]                = $(this).find('#inputSenha').val();
    cfg["sexo"]                 = $(this).find('#inputSexo').val();
    
    $.each(cfg, function(idx, val){
        if(!$.trim(val)) passou = false;
    });
    
    if( cfg["nome"].length < 3 ) {
        passou = false;
        msgs.push('nome');
    }

    if( cfg["telefone_celular"].length < 14 ) {
        passou = false;
        msgs.push('telefone');
    }

    if( cfg["email"].indexOf('@') == -1 ) {
        passou = false;
        msgs.push('e-mail');
    }

    if( cfg["senha"].length < 6 ) {
        passou = false;
        msgs.push('senha (mínimo 6 caracteres)');
    }

    if( cfg["data_nascimento"].length < 10 ) {
        passou = false;
        msgs.push('data de nascimento');
    }
    
    if( passou ){
        clientesClass.saveClientePerfil(cfg);
    } else {
        var alertMsg = $('#page-clientes-incluir .form-clientes-incluir .alert');

        alertMsg.find( '.especification' ).text( msgs.join(', ') );
        alertMsg.show();
        $(document).scrollTop(0);

        if( typeof cordova !== 'undefined' ){
            window.plugins.toast.showLongBottom( 'Preencha os campos corretamente.' );
        }
    }
});

$(document).on('change', '#page-clientes-incluir #inputSexo', function(e){
    if(!$('#page-clientes-incluir .media img').hasClass('camera')){
        if($(this).val() === "M") $('#page-clientes-incluir .media img').attr('src', clientesClass.defaultPhotoM);
        if($(this).val() === "F") $('#page-clientes-incluir .media img').attr('src', clientesClass.defaultPhotoF);
    }
});

$(document).on('click', '#page-clientes-lista .btn-editar', function(e){
    e.preventDefault();
    
    localStorage.setItem('id_cliente', $(this).parent().data("id"));
    
    clientesClass.beginClientEdit(false);
});

$(document).on('click', '#page-vendas .btn-sel-venda', function(e){
    e.preventDefault();
    
    var id_cliente = $(this).attr('data-id');
    
    $('#page-clientes-lista table tr .acoes[data-id=' + id_cliente + ']').parent().find('.infos .sel-cliente').trigger('click');
});


$(document).on('click', '#page-clientes-lista .sel-cliente', function(e){
    e.preventDefault();
    
    var obj = $(this).parent().parent();
    
    clientesClass.cliente.id_cliente = obj.find('.acoes').data("id");
    clientesClass.cliente.nome       = obj.find('.infos .nome').html();
    clientesClass.cliente.foto       = obj.find('.foto img').attr('src');
    clientesClass.cliente.cartelas   = obj.find('.acoes').data("cartela");
    
    localStorage.setItem('id_cliente', clientesClass.cliente.id_cliente);
    
    carrinhoClass.getParametrosVenda(function(ret1, ret2){ 
        produtosClass.getTagsProdutos(function(){
            carrinhoClass.initCarrinho(false); 
        }); 
    }, false);
    
    $('#page-produtos .mini-carrinho .header img').attr('src', clientesClass.cliente.foto);
    $('#page-produtos .mini-carrinho .header h3').html(clientesClass.cliente.nome);
    $('#page-produtos .mini-carrinho .header h4 > span').html(clientesClass.cliente.cartelas);
    
    produtosClass.beginProdutosLista(false);
});

$(document).on('click', '#page-clientes-editar .nav li > a[href="#bio"]', function(e){
    clientesClass.processClientsBioList();
});

$(document).on('click', '#page-clientes-editar .table .view-bio', function(e){
    e.preventDefault();
    
    var id = $(this).parent().parent().attr('data-id');
    
    $('#page-clientes-editar .lista-bios .table tbody tr.line-details[data-id="' + id + '"]').toggleClass('hidden');
});

$(document).on('click', '#page-clientes-editar .table .excluir-bio', function(e){
    e.preventDefault();
    
    if(confirm("Tem certeza que deseja excluir esse item?")){
        clientesClass.removeClientsBio($(this).parent().parent().attr('data-id'));
    }
});

$(document).on('click', '#page-clientes-editar .table .editar-bio', function(e){
    e.preventDefault();

    $('#page-clientes-editar .form-clientes-bio .panel .photo').remove();
    $('#page-clientes-editar .form-clientes-bio').hide('fast');
    $('#page-clientes-editar .lista-bios').show('fast');
    
    clientesClass.beginClientBioEdit($(this).parent().parent().attr('data-id'));
});

$(document).on('click', '#page-clientes-editar .form-clientes-bio .btn-cancelar', function(e){
    e.preventDefault();

    $('#page-clientes-editar .form-clientes-bio .panel .photo').remove();
    $('#page-clientes-editar .form-clientes-bio').hide('fast');
    $('#page-clientes-editar .lista-bios').show('fast');
    
    clientesClass.beginClientBioEdit($(this).parent().parent().attr('data-id'));
});

$(document).on('keyup', '#page-clientes-editar .form-clientes-bio #inputPeso, #page-clientes-editar .form-clientes-bio #inputAltura', function(e){
    var valPeso   = $('#page-clientes-editar .form-clientes-bio #inputPeso').val();
    var valAltura = $('#page-clientes-editar .form-clientes-bio #inputAltura').val();
    
    valPeso   = parseFloat(valPeso.replace(',', '.'));
    valAltura = parseFloat(valAltura.replace(',', '.'));
    
    if(valPeso && valAltura){
        var valIMC = valPeso / (valAltura * valAltura);
        $('#page-clientes-editar .form-clientes-bio #inputImc').val(valIMC.toFixed(2));
    } else {
        $('#page-clientes-editar .form-clientes-bio #inputImc').val('0')
    }
});



$(document).on('submit', '#page-clientes-editar .form-clientes-editar', function(e){
    e.preventDefault();
    
    $('#page-clientes-editar .alert').hide();
    
    var passou = true,
        cfg = {},
        msgs = [];
    
    cfg["nome"]                 = $(this).find('#inputNome').val();
    cfg["email"]                = $(this).find('#inputEmail').val();
    cfg["foto"]                 = $(this).find('.media img').attr('src');
    cfg["id_anfitriao"]         = $(this).find('#inputAnfitriao').val();
    cfg["telefone_celular"]     = $(this).find('#inputTelefone').val();
    cfg["data_nascimento"]      = $(this).find('#inputDataNascimento').val();
    cfg["senha"]                = $(this).find('#inputSenha').val();
    cfg["sexo"]                 = $(this).find('#inputSexo').val();
    cfg["estrelas"]             = $(this).find('#inputEstrelas').val();
    
    $.each(cfg, function(idx, val){
        if( !$.trim( val ) ) {
            passou = false;
        }
    });

    if( cfg["nome"].length < 3 ) {
        passou = false;
        msgs.push('nome');
    }

    if( cfg["telefone_celular"].length < 14 ) {
        passou = false;
        msgs.push('telefone');
    }

    if( cfg["email"].indexOf('@') == -1 ) {
        passou = false;
        msgs.push('e-mail');
    }

    if( cfg["senha"].length < 6 ) {
        passou = false;
        msgs.push('senha (mínimo 6 caracteres)');
    }

    if( cfg["data_nascimento"].length < 10 ) {
        passou = false;
        msgs.push('data de nascimento');
    }

    if( cfg["estrelas"].length < 1 ) {
        passou = false;
        msgs.push('estrelas');
    }
    
    if( passou ){
        clientesClass.saveClientePerfil( cfg );
    } else {
        var alertMsg = $('#page-clientes-editar .form-clientes-editar .alert');

        alertMsg.find( '.especification' ).text( msgs.join(', ') );
        alertMsg.show();
        $(document).scrollTop(0);

        if( typeof cordova !== 'undefined' ){
            window.plugins.toast.showLongBottom( 'Preencha os campos corretamente.' );
        }
    }
});


$(document).on('click', '#page-clientes-editar .lista-bios .btn-adicionar', function(e){
    e.preventDefault();
    
    $('#page-clientes-editar .form-clientes-bio')[0].reset();
    $('#page-clientes-editar .form-clientes-bio .panel .photo').remove();
    
    $(this).parent().hide('fast');
    $(this).parent().parent().find('.form-clientes-bio').show('fast');
});

$(document).on('click', '#page-clientes-editar .form-clientes-bio .panel .btn-add-photo', function(e){
    e.preventDefault();    

    clientesClass.cameraTakePhoto(clientesClass.takePhotoBio);
});

$(document).on('click', '#page-clientes-editar .form-clientes-bio .panel .btn-drop', function(e){
    e.preventDefault();
    e.stopPropagation();

    if(confirm("Tem certeza que deseja apagar essa foto?")){
        if($(this).parent().hasClass('add')){
            $(this).parent().remove();
        } else {
            $(this).parent().addClass('remove').hide();
        }
    }
});

$(document).on('click', '#page-clientes-editar .form-clientes-bio .panel .photo > a, #page-clientes-editar .lista-bios .table tbody tr.line-details td .photo > a', function(e){
    e.preventDefault();
    e.stopPropagation();
    
    $(this).parent().toggleClass('expand');
});

$(document).on('submit', '#page-clientes-editar .form-clientes-bio', function(e){
    e.preventDefault();
    
    $('#page-clientes-editar .alert').hide();
    
    var passou = true,
        cfg = {},
        msgs = [];
    
    cfg["token"]                = $(this).find('#token').val();
    
    cfg["medidas_peso"]         = $(this).find('#inputPeso').val();
    cfg["medidas_altura"]       = $(this).find('#inputAltura').val();
    cfg["medidas_torax"]        = $(this).find('#inputTorax').val();
    cfg["medidas_cintura"]      = $(this).find('#inputCintura').val();
    cfg["medidas_barriga"]      = $(this).find('#inputBarriga').val();
    cfg["medidas_quadril"]      = $(this).find('#inputQuadril').val();
    
    cfg["bio_imc"]              = $(this).find('#inputImc').val();
    cfg["bio_perc_gordura"]     = $(this).find('#inputPercGordura').val();
    cfg["bio_perc_musculo"]     = $(this).find('#inputPercMuscular').val();
    cfg["bio_metabolismo"]      = $(this).find('#inputMetabolismo').val();
    cfg["bio_gordura_visceral"] = $(this).find('#inputGorduraVisceral').val();
    cfg["bio_idade_corporal"]   = $(this).find('#inputIdadeCorporal').val();
    
    cfg["bio_fotos_add"]        = [];
    $(this).find('.panel .photo.add > a').each(function(){
        cfg["bio_fotos_add"].push(cleanImgData($(this).css('background-image')));
    });
    
    cfg["bio_fotos_del"]        = [];
    $(this).find('.panel .photo.remove > span').each(function(){
        cfg["bio_fotos_del"].push($(this).attr('data-id'));
    });
    
    $.each(cfg, function(idx, val){
        if(!$.trim(val) && idx != "token" && idx != "bio_fotos_add" && idx != "bio_fotos_del") passou = false; 
    });

    if( cfg["medidas_peso"].length < 1 ) {
        passou = false;
        msgs.push( 'peso' );
    }

    if( cfg["medidas_altura"].length < 1 ) {
        passou = false;
        msgs.push( 'altura' );
    }

    if( cfg["medidas_torax"].length < 1 ) {
        passou = false;
        msgs.push( 'torax' );
    }

    if( cfg["medidas_cintura"].length < 1 ) {
        passou = false;
        msgs.push( 'cintura' );
    }

    if( cfg["medidas_barriga"].length < 1 ) {
        passou = false;
        msgs.push( 'barriga' );
    }

    if( cfg["medidas_quadril"].length < 1 ) {
        passou = false;
        msgs.push( 'quadril' );
    }

    if( cfg["bio_perc_gordura"].length < 1 ) {
        passou = false;
        msgs.push( 'gordura' );
    }

    if( cfg["bio_perc_musculo"].length < 1 ) {
        passou = false;
        msgs.push( 'musculo' );
    }

    if( cfg["bio_metabolismo"].length < 1 ) {
        passou = false;
        msgs.push( 'metabolismo' );
    }

    if( cfg["bio_gordura_visceral"].length < 1 ) {
        passou = false;
        msgs.push( 'gordura visceral' );
    }

    if( cfg["bio_idade_corporal"].length < 1 ) {
        passou = false;
        msgs.push( 'idade corporal' );
    }
    
    if(passou){
        clientesClass.saveClienteBio(cfg);
    } else {
        var alertMsg = $('#page-clientes-editar .form-clientes-bio .alert');

        alertMsg.find( '.especification' ).text( msgs.join(', ') );
        alertMsg.show();
        $(document).scrollTop( 0 );

        if( typeof cordova !== 'undefined' ){
            window.plugins.toast.showLongBottom( 'Preencha os campos corretamente.' );
        }
    }
});


$(document).on('click', '#page-produtos .lista-produtos .tags a', function(e){
    e.preventDefault();
    
    var tag = $(this);
    var id_tag = tag.attr("data-id");
    
    if(debugDev) console.log("Selecionou tag: " + id_tag);
    
    produtosClass.renderSubTags(function(){
        $('#page-produtos .lista-produtos .itens .grupo .item').hide();
        
        if(tag.attr('data-auto') != "false"){
            if(typeof produtosClass.arrTagsProdutos[id_tag] != "undefined"){
                $.each(produtosClass.arrTagsProdutos[id_tag], function(idx, id_produto){
                    $('#page-produtos .lista-produtos .itens .grupo .item[data-id="' + id_produto + '"]').show();
                });
            }
        }
        
        $('#page-produtos .lista-produtos .subtags li a').each(function(){
            if($(this).attr('data-auto') != "false"){
                if(typeof produtosClass.arrTagsProdutos[$(this).attr('data-id')] != "undefined"){
                    $.each(produtosClass.arrTagsProdutos[$(this).attr('data-id')], function(idx, id_produto){
                        $('#page-produtos .lista-produtos .itens .grupo .item[data-id="' + id_produto + '"]').show();
                    });
                }
            }
        });
        
        $('#page-produtos .lista-produtos .tags li').removeClass('active');
        tag.parent().addClass('active');
    }, id_tag);
});

$(document).on('click', '#page-produtos .lista-produtos .subtags a', function(e){
    e.preventDefault();
    
    var id_tag = $(this).data("id");
    
    if(debugDev) console.log("Selecionou subtag: " + id_tag);
    
    $('#page-produtos .lista-produtos .itens .grupo .item').hide();
    
    if(typeof produtosClass.arrTagsProdutos[id_tag] != "undefined"){
        $.each(produtosClass.arrTagsProdutos[id_tag], function(idx, id_produto){
            $('#page-produtos .lista-produtos .itens .grupo .item[data-id="' + id_produto + '"]').show();
        });
    }
    
    $('#page-produtos .lista-produtos .subtags li').removeClass('active');
    $(this).parent().addClass('active');
});


$(document).on('click', '#page-produtos .lista-produtos .itens .grupo .item', function(e){
    e.preventDefault();
    
    if(debugDev) console.log("Produto selecionado: " + $(this).data("id") + " | " + $(this).find("h3").html());
    
    if(!$(this).attr("data-grade")){
        carrinhoClass.addProduto(false, $(this).data("id"));
    } else {
        if(debugDev) console.log("Produto faz parte de uma grade de consumo.");
        gradeClass.beginProcessProdutoGrade($(this).attr("data-id"), false);
    }
});


$(document).on('click', '#page-produtos .mini-carrinho .lista table .mais-qtd', function(e){
    e.preventDefault();
    
    if(debugDev) console.log("Adicionando mais um item do produto: " + $(this).parent().parent().data("id") + " | " + $(this).parent().parent().find(".nome").html());
    
    carrinhoClass.addProduto(false, $(this).parent().parent().data("id"));
});

$(document).on('click', '#page-produtos .mini-carrinho .lista table .menos-qtd', function(e){
    e.preventDefault();
    
    if(debugDev) console.log("Removendo mais um item do produto: " + $(this).parent().parent().data("id") + " | " + $(this).parent().parent().find(".nome").html());
    
    carrinhoClass.delProduto(false, $(this).parent().parent().data("id"));
});

$(document).on('click', '#page-carrinho table .mais-qtd', function(e){
    e.preventDefault();
    
    if(debugDev) console.log("Adicionando mais um item do produto: " + $(this).parent().parent().data("id") + " | " + $(this).parent().parent().find(".nome").html());
    
    carrinhoClass.addProdutoFinal(false, $(this).parent().parent().data("id"));
});

$(document).on('click', '#page-carrinho table .menos-qtd', function(e){
    e.preventDefault();
    
    if(debugDev) console.log("Removendo mais um item do produto: " + $(this).parent().parent().data("id") + " | " + $(this).parent().parent().find(".nome").html());
    
    carrinhoClass.delProdutoFinal(false, $(this).parent().parent().data("id"));
});




$(document).on('click', '#page-grade .itens .item', function(e){
    e.preventDefault();
    
    if(debugDev) console.log("Selecionando produto para compor grade.");
    
    var total_produtos   = $(this).parent().attr("data-total");
    
    if(!$(this).hasClass('inactive')) $(this).toggleClass('selected');
    
    if($(this).parent().find('.item.selected').length == total_produtos){
        $(this).parent().find('.item').not('.selected').addClass('inactive');
        $('#page-grade .faixa .btn-avancar').css('display', 'block');
    } else {
        $(this).parent().find('.item').removeClass('inactive');
        $('#page-grade .faixa .btn-avancar').css('display', 'none');
    }
});

$(document).on('click', '#page-grade .faixa .btn-avancar', function(e){
    e.preventDefault();
    
    if(debugDev) console.log("Adicionando itens selecionados na grade.");
    
    var id_produto   = $('#page-grade .itens').attr("data-produto");
    var id_categoria = $('#page-grade .itens').attr("data-categoria");
    
    var selItens = [];
    $('#page-grade .itens .item.selected').each(function(){
        selItens.push($(this).attr('data-id'));
    });
    
    gradeClass.addItemGrade(id_produto, id_categoria, selItens);
});



$(document).on('click', '#page-produtos .mini-carrinho .btn-pagar', function(e){
    e.preventDefault();
    
    if(debugDev) console.log("Renderizando carrinho completo");
    
    carrinhoClass.beginRenderCarrinhoFinal(false);
});



$(document).on('click', '#page-carrinho .btn-pagar', function(e){
    e.preventDefault();
    
    if(carrinhoClass.carrinho.itens.length){
        if(!carrinhoClass.carrinho.valor_total){
            if(debugDev) console.log("Valor de carrinho zerado, fecha a compra direto");
            
            carrinhoClass.carrinho.forma_pagto = 9;
            carrinhoClass.beginFinalizaVenda(false);            
        } else {
            if(debugDev) console.log("Fecha carrinho e vai pro pagamento");

            carrinhoClass.beginRenderPagamento(false);
        }
    } else {
        $('#page-carrinho .lista .alert').show();
    }
});

$(document).on('change', '#page-carrinho .usar_cartela_digital', function(e){
    var id_produto_venda = $(this).parent().parent().data("id-venda");
    var usar             = $(this).is(":checked");
    var valor_venda      = $(this).parent().parent().find('.unitario.fixo span.valor').html().toString().replace('.', '').replace(',', '.');
    var quantidade       = $(this).parent().parent().find('.acoes .qtd').html().toString().replace('x', '');
    
    carrinhoClass.processUsaCartela(carrinhoClass.beginRenderCarrinhoFinal, id_produto_venda, usar, valor_venda, quantidade);
});





$(document).on('click', '#page-pagamento .btn-finalizar', function(e){
    e.preventDefault();
    
    var id_forma_pagamento = $('#page-pagamento .lista li.active a').data('id');

    if(typeof id_forma_pagamento === "undefined"){
        $('#page-pagamento .lista .alert').show();
    } else {
        carrinhoClass.carrinho.forma_pagto = id_forma_pagamento;
        carrinhoClass.beginFinalizaVenda(false);
    }
});

$(document).on('click', '#page-pagamento .lista li > a', function(e){
    e.preventDefault();
    
    if(debugDev) console.log("Forma de pagamento escolhida: " + $(this).html());
	
    $('#page-pagamento .lista li').removeClass('active');
    $(this).parent().addClass('active');
    $('#page-pagamento .lista .alert').hide();
});


