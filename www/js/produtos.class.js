var produtosClass = {
    arrTags: [],
    arrSubTags: [],
    arrTagsProdutos: [],
    arrProdutos: [],
    
    beginProdutosLista: function(ret){
        if(!ret) carrinhoClass.getParametrosVenda(produtosClass.beginProdutosLista, false);
        if(ret === "getParametrosVenda") produtosClass.getTagsProdutos(produtosClass.beginProdutosLista);
        if(ret === "getTagsProdutos") produtosClass.renderTags(produtosClass.beginProdutosLista);
        if(ret === "renderTags")      produtosClass.renderProdutos(produtosClass.beginProdutosLista);
        if(ret === "renderProdutos")  produtosClass.endProdutosLista(false);
    },
    
    endProdutosLista: function(ret){
        $('#page-produtos .tags li a').first().trigger('click');
        pagesClass.toPage('page-produtos');
    },
    
    
    getTagsProdutos: function(cb){
        execSQL("\
            SELECT  * \
            FROM    tag \
            WHERE   id_parent IS NOT NULL \
                AND id_tag IN ( \
                    SELECT DISTINCT id_tag \
                    FROM tag_produto \
                    WHERE id_produto IN ( \
                        SELECT  DISTINCT p.id_produto \
                        FROM    categoria AS c, item_tabela_precos AS itp, tabela_precos AS tpr, ponto_de_venda AS pdv, produto AS p \
                        WHERE   p.id_categoria = c.id_categoria \
                            AND p.visivel = 'true' \
                            AND tpr.id_tabela_precos = itp.id_tabela_precos \
                            AND pdv.id_classe_ponto_de_venda = tpr.id_classe_ponto_de_venda \
                            AND itp.id_produto = p.id_produto \
                            AND itp.id_uf = ? \
                            AND pdv.id_ponto_de_venda = ? \
                       GROUP BY    p.id_produto, c.id_categoria \
                    ) \
                ) \
            ORDER BY nome ASC", [ localStorage.getItem('id_uf'), localStorage.getItem('id_ponto_de_venda') ], function(tx, res){
            
            produtosClass.arrSubTags = [];

            for(var i = 0; i < res.rows.length; i++){
                if(typeof produtosClass.arrSubTags[res.rows.item(i).id_parent] == "undefined") produtosClass.arrSubTags[res.rows.item(i).id_parent] = [];
                produtosClass.arrSubTags[res.rows.item(i).id_parent].push({ "id_tag": res.rows.item(i).id_tag, "nome": res.rows.item(i).nome, "auto": res.rows.item(i).exibir_auto_produtos, "visivel": res.rows.item(i).visivel });
            }

            var arrTmp = [];
            for(var i = 0; i < res.rows.length; i++) arrTmp.push(res.rows.item(i).id_parent);
            var listaTags = arrTmp.join();
            
            execSQL("\
                SELECT  * \
                FROM    tag \
                WHERE   id_tag IN (" + listaTags + ") \
                ORDER BY nome ASC", [], function(tx, res){

                produtosClass.arrTags = [];

                for(var i = 0; i < res.rows.length; i++){
                    produtosClass.arrTags.push({ "id_tag": res.rows.item(i).id_tag, "nome": res.rows.item(i).nome, "auto": res.rows.item(i).exibir_auto_produtos, "visivel": res.rows.item(i).visivel });
                }

                execSQL("\
                    SELECT id_tag, id_produto \
                    FROM tag_produto \
                    ORDER BY id_tag ASC", [], function(tx, res){

                    produtosClass.arrTagsProdutos = [];

                    for(var i = 0; i < res.rows.length; i++){
                        if(typeof produtosClass.arrTagsProdutos[res.rows.item(i).id_tag] == "undefined") produtosClass.arrTagsProdutos[res.rows.item(i).id_tag] = [];
                        produtosClass.arrTagsProdutos[res.rows.item(i).id_tag].push(res.rows.item(i).id_produto);
                    }

                    execSQL("\
                        SELECT  c.id_categoria, c.nome as nome_categoria, \
                                MAX(g.id_grade_consumo) AS grade, \
                                p.id_produto, p.nome, p.apelido, MAX(pi.imagem) AS imagem  \
                        FROM    categoria AS c, item_tabela_precos AS itp, tabela_precos AS tpr, ponto_de_venda AS pdv, produto AS p \
                        LEFT JOIN   produto_imagem AS pi ON pi.id_produto = p.id_produto \
                        LEFT JOIN   grade_consumo AS g ON g.id_produto = p.id_produto \
                        WHERE   p.id_categoria = c.id_categoria \
                            AND p.visivel = 'true' \
                            AND tpr.id_tabela_precos = itp.id_tabela_precos \
                            AND pdv.id_classe_ponto_de_venda = tpr.id_classe_ponto_de_venda \
                            AND itp.id_produto = p.id_produto \
                            AND itp.id_uf = ? \
                            AND pdv.id_ponto_de_venda = ? \
                        GROUP BY    p.id_produto \
                        ORDER BY    c.nome, p.apelido", [ localStorage.getItem('id_uf'), localStorage.getItem('id_ponto_de_venda') ], function(tx, res){

                        produtosClass.arrProdutos = [];

                        for(var i = 0; i < res.rows.length; i++){
                            produtosClass.arrProdutos.push(res.rows.item(i));
                        }

                        if(debugDev)  console.log("Recuperando lista de tags e produtos");
                        if(debugData) console.log(produtosClass.arrTags);
                        if(debugData) console.log(produtosClass.arrProdutos);

                        cb('getTagsProdutos');
                    });                 
                });                 
            });                 
        });
    },
    
    renderTags: function(cb){
        var objT = $('#page-produtos .lista-produtos .tags');
        var objP = $('#page-produtos .lista-produtos .itens .grupo');
        
        objT.html('');
        objP.html('');
        
        $.each(produtosClass.arrTags, function(idx, tag){
            if(typeof tag != "undefined"){
                objT.append('<li role="presentation" class=""><a href="#tag" class="text-uppercase" data-id="' + tag.id_tag + '" data-auto="' + tag.auto + '">' + tag.nome + '</a></li>');
            }
        });
        
        if(typeof cb == "function") cb('renderTags');
    },
    
    renderSubTags: function(cb, id_tag){
        var objST = $('#page-produtos .lista-produtos .subtags');
        
        objST.html('');
        
        $.each(produtosClass.arrSubTags[id_tag], function(idx, sub){
            if(typeof sub != "undefined"){
                objST.append('<li role="presentation" class=""><a href="#tag" class="text-uppercase" data-id="' + sub.id_tag + '" data-id-parent="' + id_tag + '" data-auto="' + sub.auto + '">' + sub.nome + '</a></li>');
            }
        });
        
        if(typeof cb == "function") cb('renderSubTags');
    },

    renderProdutos: function(cb){
        var tpl  = $('#templates .tpl-produtos-item').html();
        var objP = $('#page-produtos .lista-produtos .itens .grupo');

        objP.html('');
        
        $.each(produtosClass.arrProdutos, function(idx, item){
            var linha = tpl;

            linha = linha.replace('##ID##',       item.id_produto);
            linha = linha.replace('##CAT##',      item.id_categoria);
            linha = linha.replace('##CAT-NOME##', item.nome_categoria);
            linha = linha.replace('##IMG##',      item.imagem);
            linha = linha.replace('##GRADE##',    item.grade === null ? "" : item.grade);
            linha = linha.replace('##TITULO##',   item.apelido);

            objP.append(linha);
        });
        
        cb('renderProdutos');
    },
    
    findProduto: function(id_produto){
        var obj = false;
        
        $.each(produtosClass.arrProdutos, function(idx, produto){
            if(produto.id_produto == id_produto) obj = produto;
        });
        
        return obj;
    }
    
};