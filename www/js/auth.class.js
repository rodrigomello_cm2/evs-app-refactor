/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var authClass = {
    checkValidLogin: function(){
        if(localStorage.getItem('id_anfitriao')){
            var dt = now();
            
            execSQL("\
                SELECT  a.* \
                FROM    anfitriao AS a, ponto_de_venda AS pv \
                WHERE   a.enabled = 'true' \
                    AND a.id_ponto_de_venda = pv.id_ponto_de_venda \
                    AND (pv.data_expiracao > ? OR pv.data_expiracao IS NULL) \
                    AND (a.expires_at > ? OR a.expires_at IS NULL) \
                    AND a.expired = 'false' \
                    AND a.id_anfitriao = ?", [ dt, dt, localStorage.getItem('id_anfitriao') ], function (tx, results){

                if(!results.rows.length) authClass.processLogoff();
            });
        }
    },
    
    processLogin: function(login, password){
        if(window.syncLock) return false;
        window.syncLock = true;
        window.syncType = "login";

        var dt = now();
        
        execSQL("\
            SELECT  a.* \
            FROM    anfitriao AS a, ponto_de_venda AS pv \
            WHERE   a.enabled = 'true' \
                AND a.id_ponto_de_venda = pv.id_ponto_de_venda \
                AND (pv.data_expiracao > ? OR pv.data_expiracao IS NULL) \
                AND (a.expires_at > ? OR a.expires_at IS NULL) \
                AND a.expired = 'false' \
                AND LOWER(a.username) = LOWER(?) \
                AND a.password = ? || '{' || salt || '}'", [ dt, dt, login, password ], function (tx, results){

            if(results.rows.length === 1){
                if(debugDev) console.log("Logado com sucesso localmente");

                var ret = { 'id_anfitriao': results.rows.item(0).id_anfitriao, 'id_ponto_de_venda': results.rows.item(0).id_ponto_de_venda };
                
                authClass.afterLoginSuccess(ret, login, password);
            } else {
                if(isOnline()){
                    authClass.remoteLogin(login, password, authClass.afterLoginSuccess, authClass.afterLoginError);
                } else {
                    authClass.afterLoginError();
                }
            }
        });
    },

    afterLoginSuccess: function(data, login, password ){
        window.syncLock = false;
        
        localStorage.setItem('login',             login);
        localStorage.setItem('senha',             password);
        localStorage.setItem('id_anfitriao',      data.id_anfitriao);
        localStorage.setItem('id_ponto_de_venda', data.id_ponto_de_venda);
        
        window.isLogged = true;
        
        syncClass.processPopulateTables();
        
        $('.navbar #btn-menu').show();
        
        var tempInt = false
        tempInt = setInterval(function(){
            if(!window.syncLock){
                pagesClass.toPage("page-clientes-lista", false, clientesClass.processClientsList);
                clearInterval(tempInt);
                tempInt = false;
            }
        }, 500);
    },

    afterLoginError: function(){
        window.syncLock = false;
        window.syncHide = false;
        window.isLogged = false;
        
        authClass.processLogoff(false);
        
        if(debugDev) console.log("Houve um erro ao tentar fazer login");
        
        $('#page-login .alert').show('fast');
    },

    remoteLogin: function(login, password, callbackSuccess, callbackError){
        if(debugDev) console.log("Autenticando no servidor remoto");

        try {
            $.ajax({
                url: urlRemote + "/autentica", 
                timeout: 3000,

                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization', 'Basic ' + btoa(login + ':' + password));
                },

                success: function (dados){
                    if(debugDev)  console.log("Usuario autenticado remotamente");
                    if(debugData) console.log("Autenticacao: " + JSON.stringify(dados));
                },

                error: function(a,b,c){
                    if(debugDev) console.log("Erro na autenticacao remota: " + JSON.stringify(a) + " | " + b + " | " + c);
                    if(typeof callbackError === "function") callbackError();
                },

                statusCode: {
                    401: function(){
                        if(typeof callbackError === "function") callbackError();
                    },

                    200: function(dados){
                        if(debugDev) console.log("Forçando a atualização das tabelas");
                        
                        execSQL("UPDATE controle SET dta = '2016-01-01 00:00:00', atualizar = 1", [], function(){
                            if(typeof callbackSuccess === "function") callbackSuccess(dados, login, password);
                        });
                    }
                }
            });
        } catch(e){
            alert("deu ruim: " + JSON.stringify(e));
        }
    }, 
    
    processLogoff: function(render){
        var render = typeof render == "undefined" ? true : render;
        
        window.syncLock = true;
        window.syncHide = false;
        window.syncType = "logoff";
        
        localStorage.removeItem('login');
        localStorage.removeItem('senha');
        localStorage.removeItem('id_anfitriao');
        localStorage.removeItem('id_ponto_de_venda');
        localStorage.removeItem('id_cliente');
        
        clearInterval(timeSyncTimer);
        timeSyncTimer = false;
        
        window.isLogged = false;
        
        window.syncLock = false;
        
        $('.navbar #btn-menu').hide();
        
        if(debugDev) console.log("Logoff realizado");

        if(render) pagesClass.toPage("page-login");
    }
};