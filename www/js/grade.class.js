
var gradeClass = {
    beginProcessProdutoGrade: function(id_produto, ret){
        // pega todos os itens componentes da grade para o produto
        execSQL("\
            SELECT  gc.id_grade_consumo, \
                    gc.id_categoria, \
                    gc.id_produto_especifico, \
                    gc.descricao, \
                    gc.quantidade AS qtd_padrao, \
                    gcp.quantidade AS qtd_local \
            FROM    grade_consumo AS gc \
            LEFT JOIN   grade_consumo_pdv AS gcp ON gc.id_grade_consumo = gcp.id_grade_consumo AND gcp.ativo = 'true' AND gcp.id_ponto_de_venda = ? \
            WHERE   gc.id_produto = ? \
            ORDER BY gc.id_grade_consumo ASC", [ localStorage.getItem('id_ponto_de_venda'), id_produto ], function(tx, res){
            
            carrinhoClass.carrinho.itensGrade[id_produto] = [];
            
            for(i=0; i<res.rows.length; i++){
                var itemGrade = {};
                
                var qtd = res.rows.item(i).qtd_local ? res.rows.item(i).qtd_local : res.rows.item(i).qtd_padrao;
                
                itemGrade.id_grade_consumo = res.rows.item(i).id_grade_consumo;
                itemGrade.id_categoria     = res.rows.item(i).id_categoria;
                itemGrade.id_produto       = res.rows.item(i).id_produto_na_venda ? res.rows.item(i).id_produto_na_venda : res.rows.item(i).id_produto_especifico;
                itemGrade.descricao        = res.rows.item(i).descricao;
                itemGrade.quantidade       = qtd;
                itemGrade.isGrade          = true;
                
                carrinhoClass.carrinho.itensGrade[id_produto].push(itemGrade);
            }
            
            gradeClass.processEscolhaProdutos(id_produto, false);
        });
    },
    
    processEscolhaProdutos: function(id_produto, ret){
        var achou = false;
        
        $.each(carrinhoClass.carrinho.itensGrade[id_produto], function(idx, item){
            if(!item.id_produto){
                if(debugDev) console.log("Renderizando produtos para grade: grade: " + item.id_grade_consumo + " | categoria: " + item.id_categoria);
                
                achou = true;
                gradeClass.populateListaProdutos(id_produto, item.id_categoria, item.id_grade_consumo, item.descricao);
                return false;
            }
        });
        
        if(!achou) gradeClass.endProcessProdutoGrade(id_produto, false);
    },
    
    populateListaProdutos: function(id_produto, id_categoria, id_grade_consumo, descricao){
        var produtos = $('#page-produtos .lista-produtos .itens .grupo .item[data-categoria=' + id_categoria + ']');
        
        var descricao = (typeof descricao == "undefined" || !$.trim(descricao) ? produtos.attr('data-categoria-nome') : descricao);

        var contProdutos = 0;
        $.each(carrinhoClass.carrinho.itensGrade[id_produto], function(idx, item){
            if(!item.id_produto && item.id_categoria == id_categoria) contProdutos++;
        });        
        
        $('#page-grade .faixa .btn-avancar').css('display', 'none');
        $('#page-grade .nome-categoria').html(descricao);
        $('#page-grade .itens').attr('data-produto', id_produto).attr('data-item', id_grade_consumo).attr('data-categoria', id_categoria).attr('data-total', contProdutos).html('');
        produtos.clone().appendTo('#page-grade .itens').show();
        
        pagesClass.toPage("page-grade");
    },
    
    addItemGrade: function(id_produto, id_categoria, selItens){
        $.each(selItens, function(i, id_item){
            $.each(carrinhoClass.carrinho.itensGrade[id_produto], function(idx, item){
                if(item.id_categoria == id_categoria && !item.id_produto){
                    if(debugDev) console.log("Adicionando a grade: id_produto: " + id_produto + " | id_categoria: " + id_categoria + " | id_item: " + id_item);

                    carrinhoClass.carrinho.itensGrade[id_produto][idx].id_produto = id_item;
                    return false;
                }
            });
        });
        
        gradeClass.processEscolhaProdutos(id_produto, false);
    },
    
    
    endProcessProdutoGrade: function(id_produto, ret){
        if(debugDev) console.log("Todos os produtos da grade foram selecionados.");
        
        carrinhoClass.addProduto(false, id_produto);
    }
    
};