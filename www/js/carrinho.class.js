var carrinhoClass = {
    carrinhoTPL: {
        id_cliente:   false,
        id_venda:     false,
        valor_total:  0,
        cartelas:     0,
        forma_pagto:  false,
        itens:        [],
        itensGrade:   []
    },
    
    itemTPL: {
        id_venda_produto: false,
        id_produto:       false,
        quantidade:       0,
        grade:            false,
        valor_unitario:   0,
        valor_custo:      0,
        valor_total:      0,
        ponto_valor:      0, 
        usa_cartela:      false
    },
    
    itemGradeTPL: {
        id_grade_consumo: false,
        id_categoria:     false,
        id_produto:       false, 
        quantidade:       0
    },
    
    resetCarrinho: function(){
        if(debugDev) console.log("Zerando carrinho atual");
        
        carrinhoClass.carrinho.id_cliente   = localStorage.getItem('id_cliente');
        carrinhoClass.carrinho.id_venda     = false;
        carrinhoClass.carrinho.valor_total  = false;
        carrinhoClass.carrinho.cartelas     = 0;
        carrinhoClass.carrinho.forma_pagto  = false;
        carrinhoClass.carrinho.itens        = [];
        carrinhoClass.carrinho.itensGrade   = [];
    },
    
    carrinho: {},
   
    initCarrinho: function(ret){
        if(ret === false){
            window.syncLock = true;
            carrinhoClass.resetCarrinho();
            carrinhoClass.getParametrosVenda(carrinhoClass.initCarrinho, false);
        }
        
        if(ret === "getParametrosVenda") carrinhoClass.setCarrinhoVenda(carrinhoClass.initCarrinho, false);
        
        if(ret && ret !== "getParametrosVenda"){
            carrinhoClass.beginRenderCarrinho(false);
            window.syncLock = false;
        }
    },
        
    addProduto: function(ret, id_produto){
        if(!ret)                         carrinhoClass.getParametrosVenda(carrinhoClass.addProduto, id_produto);
        if(ret === "getParametrosVenda") carrinhoClass.setCarrinhoVenda(carrinhoClass.addProduto, id_produto);
        if(ret === "loadVendaItens")     carrinhoClass.addItem(carrinhoClass.addProduto, id_produto);
        if(ret === "setCarrinhoVenda")   carrinhoClass.addItem(carrinhoClass.addProduto, id_produto);
        if(ret === "saveItem")           carrinhoClass.beginRenderCarrinho(false, true);
        if(ret === "saveItensGrade")     carrinhoClass.beginRenderCarrinho(function(){ pagesClass.toPage("page-produtos"); }, true);
    },
    
    delProduto: function(ret, id_produto){
        if(!ret)                     carrinhoClass.delItem(carrinhoClass.delProduto, id_produto);
        if(ret === "saveItem")       carrinhoClass.beginRenderCarrinho(false, true);
        if(ret === "saveItensGrade") carrinhoClass.beginRenderCarrinho(false, true);
    },
    
    addProdutoFinal: function(ret, id_produto){
        if(!ret)                         carrinhoClass.getParametrosVenda(carrinhoClass.addProdutoFinal, id_produto);
        if(ret === "getParametrosVenda") carrinhoClass.setCarrinhoVenda(carrinhoClass.addProdutoFinal, id_produto);
        if(ret === "loadVendaItens")     carrinhoClass.addItem(carrinhoClass.addProdutoFinal, id_produto);
        if(ret === "setCarrinhoVenda")   carrinhoClass.addItem(carrinhoClass.addProdutoFinal, id_produto);
        if(ret === "saveItem")           carrinhoClass.beginRenderCarrinhoFinal(false);
        if(ret === "saveItensGrade")     carrinhoClass.beginRenderCarrinhoFinal(false);
    },
    
    delProdutoFinal: function(ret, id_produto, callback){
        if(!ret) {
            carrinhoClass.delItem(function( retCb, id_produtoCb ) {
                carrinhoClass.delProdutoFinal( retCb, id_produtoCb, callback );
            }, id_produto);
        }

        if(ret === "saveItem") {
            carrinhoClass.beginRenderCarrinhoFinal(false, function(){
                if( typeof callback == 'function' ) {
                    callback();
                }
            });
        }

        if(ret === "saveItensGrade") {
            carrinhoClass.beginRenderCarrinhoFinal(false, function(){
                if( typeof callback == 'function' ) {
                    callback();
                }
            });
        }
    },
    
    getParametrosVenda: function(cb, id_produto){
        if(debugDev) console.log("Recuperando parametros de venda");
        
        if(localStorage.getItem('id_uf') && localStorage.getItem('faixa')){
            if(typeof cb == "function") cb("getParametrosVenda", id_produto);
        } else {
            execSQL("\
                SELECT  a.faixa, pdv.id_uf \
                FROM    anfitriao AS a, ponto_de_venda AS pdv \
                WHERE   a.id_ponto_de_venda = pdv.id_ponto_de_venda \n\
                    AND a.id_anfitriao = ?", [ localStorage.getItem('id_anfitriao') ], function(tx, res) {
                
                localStorage.setItem('id_uf', res.rows.item(0).id_uf);
                localStorage.setItem('faixa', res.rows.item(0).faixa);

                if(typeof cb == "function") cb("getParametrosVenda", id_produto);
            });
        }
    },
    
    setCarrinhoVenda: function(cb, id_produto){
        var id_cliente   = carrinhoClass.carrinho.id_cliente;
        var id_anfitriao = localStorage.getItem('id_anfitriao');
        
        if(!carrinhoClass.carrinho.id_venda){
            if(debugDev) console.log("Inicializando carrinho");
            
            execSQL("\
                SELECT  id_venda \
                FROM    venda \
                WHERE   id_cliente = ? \
                    AND SUBSTR(data_fim, 1, 4) = '1970'\
                LIMIT   1", [ id_cliente ], function(tx, res) {

                var dt = now();
                
                if(!res.rows.length){
                    if(id_produto){
                        var id_venda = carrinhoClass.carrinho.id_venda = guid();

                        carrinhoClass.carrinho.id_venda = id_venda;
                    
                        if(debugDev) console.log("Inicializando nova venda: " + id_venda);

                        execSQL("\
                            INSERT INTO venda \n\
                                (id_venda, id_cliente, data_inicio, data_fim, status, id_anfitriao) \
                            VALUES \
                                (?, ?, ?, ?, ?, ?)", [ id_venda, id_cliente, dt, '1970-01-01 00:00:00', 0, id_anfitriao ], function(){

                            execSQL("UPDATE controle SET atualizar = 1 WHERE tabela = 'venda'", [ ], function(tx, res){
                                syncClass.processPopulateTables();
                                cb("setCarrinhoVenda", id_produto);
                            });
                        });
                    } else {
                        cb("setCarrinhoVenda", id_produto);
                    }
                } else {
                    var id_venda = res.rows.item(0).id_venda;
                    
                    carrinhoClass.carrinho.id_venda = id_venda;
                    
                    if(id_produto){
                        if(debugDev) console.log("Atualizando venda pré existente: " + id_venda);

                        execSQL("\
                            UPDATE  venda \
                            SET data_inicio = ?, \
                                dta = null \
                            WHERE   id_venda = ? \
                                AND id_cliente = ? \
                                AND SUBSTR(data_fim, 1, 4) = '1970'", [ dt, id_venda, id_cliente ], function(){

                            execSQL("UPDATE controle SET atualizar = 1 WHERE tabela = 'venda'", [ ], function(tx, res){
                                syncClass.processPopulateTables();
                                cb("setCarrinhoVenda", id_produto);
                            });
                        });
                    } else {
                        carrinhoClass.loadVendaItens(cb, id_produto, carrinhoClass.carrinho.id_venda);
                    }
                }
            });
            
        } else {
            carrinhoClass.loadVendaItens(cb, id_produto, carrinhoClass.carrinho.id_venda);
        }
    },
    
    loadVendaItens: function(cb, id_produto, id_venda){
        if(debugDev) console.log("Carregando itens da venda: " + id_venda);
        
        execSQL("\
            SELECT  id_venda_produto, id_produto, id_venda, id_forma_pagamento, valor_venda, valor_custo, quantidade, pontos_de_volume, \n\
                    (SELECT MAX(id_grade_consumo) AS usa_grade FROM grade_consumo WHERE grade_consumo.id_produto = venda_produto.id_produto) AS usa_grade \
            FROM    venda_produto \
            WHERE   quantidade > 0 AND id_parent IS NULL \
                AND id_venda = ?", [ id_venda ], function(tx, res){

            var item = null;
            carrinhoClass.carrinho.itens = [];

            for(i=0; i<res.rows.length; i++){
                item = {};

                item.id_venda_produto = res.rows.item(i).id_venda_produto;
                item.id_produto       = res.rows.item(i).id_produto;
                item.quantidade       = res.rows.item(i).quantidade;
                item.grade            = res.rows.item(i).usa_grade ? true : false;
                item.valor_unitario   = res.rows.item(i).valor_venda;
                item.valor_custo      = res.rows.item(i).valor_custo;
                item.valor_total      = item.valor_unitario * item.quantidade;
                item.ponto_valor      = res.rows.item(i).pontos_de_volume;
                item.usa_cartela      = res.rows.item(i).id_forma_pagamento === 9 ? true : false;

                carrinhoClass.carrinho.itens.push(item);
            }

            item = null;
            
            cb("loadVendaItens", id_produto);
        });
        
    },
    
    searchProduto: function(id_produto){
        var achou = false;
        
        $.each(carrinhoClass.carrinho.itens, function(idx, item){
            if(item.id_produto == id_produto) achou = idx;
        });
        
        return achou;
    },
    
    addItem: function(cb, id_produto){
        if(debugDev) console.log("Iniciando adição de produto: " + id_produto);
        
        var selItem = {};
        
        var obj = produtosClass.findProduto(id_produto);
        
        // verifica se o item esta no carrinho
        var achou = carrinhoClass.searchProduto(id_produto);
        
        if(achou !== false){
            // Se não for grade incrementa a quantidade e valor total
            if( !carrinhoClass.carrinho.itens[achou].grade ){
                carrinhoClass.carrinho.itens[achou].quantidade = parseInt(carrinhoClass.carrinho.itens[achou].quantidade) + 1;
                carrinhoClass.carrinho.itens[achou].valor_total = parseFloat(carrinhoClass.carrinho.itens[achou].valor_unitario) * parseInt(carrinhoClass.carrinho.itens[achou].quantidade);
            }

            selItem = carrinhoClass.carrinho.itens[achou];
        // }

        // if(achou !== false){
            if(debugDev) console.log("Produto já existente no carrinho");

            carrinhoClass.saveItem(cb, carrinhoClass.carrinho.id_venda, selItem);
        } else {
            if(debugDev) console.log("Produto não encontrado no carrinho");
            var gradeBackup = carrinhoClass.carrinho.itensGrade[id_produto];

            var continueAdd = function(){
                execSQL("\
                    SELECT  venda AS valor_venda_padrao, iptp.preco AS valor_venda_ponto, custo" + localStorage.getItem('faixa') + " AS valor_custo, itp.ponto_valor \
                    FROM    item_tabela_precos AS itp \
                    LEFT JOIN   item_pdv_tabela_precos AS iptp ON iptp.id_produto = itp.id_produto AND iptp.ativo = 'true' AND iptp.id_ponto_de_venda = ? \
                    JOIN    tabela_precos AS tp ON tp.id_tabela_precos = itp.id_tabela_precos \
                    JOIN    ponto_de_venda AS pdv ON pdv.id_classe_ponto_de_venda = tp.id_classe_ponto_de_venda \
                    JOIN    anfitriao AS a ON a.id_ponto_de_venda = pdv.id_ponto_de_venda \
                    WHERE   a.id_anfitriao = ? \
                        AND itp.id_uf = ? \
                        AND itp.id_produto = ?", [ localStorage.getItem('id_ponto_de_venda'), localStorage.getItem('id_anfitriao'), localStorage.getItem('id_uf'), id_produto ], function(tx, res) {

                    carrinhoClass.carrinho.itensGrade[id_produto] = gradeBackup;

                    var selItem = {};

                    var id_venda_produto = guid();

                    var valorVenda = res.rows.item(0).valor_venda_ponto ? res.rows.item(0).valor_venda_ponto : res.rows.item(0).valor_venda_padrao;
                    var valorCusto = res.rows.item(0).valor_custo;
                    var pontoValor = res.rows.item(0).ponto_valor;

                    selItem.id_produto       = id_produto;
                    selItem.id_venda_produto = id_venda_produto;
                    selItem.quantidade       = 1;
                    selItem.grade            = obj.grade ? true : false;
                    selItem.valor_total      = valorVenda;
                    selItem.valor_custo      = valorCusto;
                    selItem.valor_unitario   = valorVenda;
                    selItem.ponto_valor      = pontoValor;
                    selItem.usa_cartela      = false;

                    carrinhoClass.carrinho.itens.push(selItem);

                    carrinhoClass.saveItem(cb, carrinhoClass.carrinho.id_venda, selItem);

                    selItem = null;
                    obj = null;
                });
            };

            // Se for grade deleta qualquer produto que seja grade que esteja no carrinho
            if( obj.grade ){                
                var cartItems = carrinhoClass.carrinho.itens,
                    cartLength = cartItems.length;

                for( var i = 0; i < cartLength; i++ ) {
                    if( cartItems[i].grade ) {
                        carrinhoClass.delProdutoFinal( false, cartItems[i].id_produto, continueAdd );
                        break;
                    }
                }

                if( cartLength == 0 ){
                    continueAdd();
                }
                // continueAdd();
            }else{
                continueAdd();
            }
        }
    },
    
    delItem: function(cb, id_produto){
        if(debugDev) console.log("Iniciando remocao de produto: " + id_produto);
        
        var selItem = null;

        // verifica se o item esta no carrinho
        var achou = carrinhoClass.searchProduto(id_produto);
        
        if(achou !== false){
            carrinhoClass.carrinho.itens[achou].quantidade = parseInt(carrinhoClass.carrinho.itens[achou].quantidade) - 1;
            carrinhoClass.carrinho.itens[achou].valor_total = parseFloat(carrinhoClass.carrinho.itens[achou].valor_unitario) * parseInt(carrinhoClass.carrinho.itens[achou].quantidade);

            selItem = carrinhoClass.carrinho.itens[achou];
            
            if(!carrinhoClass.carrinho.itens[achou].quantidade){
                carrinhoClass.carrinho.itens.splice(achou, 1);
                // carrinhoClass.carrinho.itensGrade.splice(id_produto, 1); // Na próxima etapa ele precisa fazer o replace desses itens, então não podem ser apagados
            }
        }
        
        carrinhoClass.carrinho.valor_total = 0;
        $.each(carrinhoClass.carrinho.itens, function(idx, item){
            carrinhoClass.carrinho.valor_total+= item.valor_total;
        });
        
        if(selItem) carrinhoClass.saveItem(cb, carrinhoClass.carrinho.id_venda, selItem);
    },
    
    saveItem: function(cb, id_venda, item){
        if(debugDev) console.log("Salvando produto em banco de dados: " + item.id_venda_produto);
        
        var id_forma_pagamento = null;
        
        if(item.usa_cartela) id_forma_pagamento = 9;
        
        if(item.quantidade == 0 && item.usa_cartela){
            carrinhoClass.carrinho.cartelas--;
        } else {
            carrinhoClass.carrinho.cartelas++;
        }
        
        execSQL("\
            INSERT OR REPLACE INTO venda_produto \
                (id_venda_produto, id_forma_pagamento, id_produto, id_venda, valor_venda, valor_custo, quantidade, pontos_de_volume, dta) \
            VALUES \
                (?, ?, ?, ?, ?, ?, ?, ?, ?)", [ item.id_venda_produto, id_forma_pagamento, item.id_produto, id_venda, item.valor_unitario, item.valor_custo, item.quantidade, item.ponto_valor, null ], function(){
            
            execSQL("UPDATE controle SET atualizar = 1 WHERE tabela = 'venda_produto'", [ ], function(tx, res){
                if(item.grade){
                    carrinhoClass.saveItensGrade(function(){ 
                        //syncClass.processPopulateTables(); 
                        cb("saveItensGrade", item.id_produto); 
                    }, item.id_venda_produto, item.id_produto, id_forma_pagamento, item.quantidade, false);
                } else {
                    //syncClass.processPopulateTables();
                    cb("saveItem", item.id_produto, false);
                }
            });
        });
    },
    
    
    saveItensGrade: function(cb, id_parent, id_produto, id_forma_pagamento, qtdHead, ret){
        var cmds = [];
        
        cmds.push({ 'sql': "UPDATE venda_produto SET quantidade = 0, dta = NULL WHERE id_parent = ?", 'values': [ id_parent ] });
        
        $.each(carrinhoClass.carrinho.itensGrade[id_produto], function(idx, item){
            item.quantidade = parseFloat(item.quantidade);
            qtdHead         = parseFloat(qtdHead);
            
            var sql = "\
                INSERT OR REPLACE INTO venda_produto \
                    SELECT  ? AS id_venda_produto, itp.id_produto, ? AS id_venda, ? AS id_forma_pagamento, ? AS quantidade, ? AS valor_venda, \
                            custo" + localStorage.getItem('faixa') + " AS valor_custo, ? AS id_parent, ? AS id_grade_consumo, \
                            ? AS quantidade1, (custo" + localStorage.getItem('faixa') + " * ?) AS valor_custo1, itp.ponto_valor AS pontos_de_valor, ? AS dta \
                    FROM    item_tabela_precos AS itp \
                    JOIN    tabela_precos AS tp ON tp.id_tabela_precos = itp.id_tabela_precos \
                    JOIN    ponto_de_venda AS pdv ON pdv.id_classe_ponto_de_venda = tp.id_classe_ponto_de_venda \
                    WHERE   pdv.id_ponto_de_venda = ? \
                        AND itp.id_uf = ? \
                        AND itp.id_produto = ?";
                
            var param = [ guid(), carrinhoClass.carrinho.id_venda, id_forma_pagamento, qtdHead, 0, id_parent, item.id_grade_consumo, 
                          (item.quantidade * qtdHead), (item.quantidade * qtdHead), null, 
                          localStorage.getItem('id_ponto_de_venda'), localStorage.getItem('id_uf'), item.id_produto ];
            
            cmds.push({ 'sql': sql, 'values': param });
            
            if(debugDev)  console.log("Item de grade: produto: " + item.id_produto + " | qtdHead: " + qtdHead + " | qtdItem: " + (item.quantidade * qtdHead));
            if(debugData) console.log(param);
        });
        
        cmds.push({ 'sql': "UPDATE controle SET atualizar = 1 WHERE tabela = 'venda_produto'", 'values': [ ] });
        
        execSQLBatch(0, cmds, cb);
    },
    
    
    beginRenderCarrinho: function(cb, ret){
        var objC  = $('#page-produtos .mini-carrinho .lista table tbody');
        var tpl   = $('#templates .tpl-mini-carrinho-item tbody').html();
        
        $('#page-produtos .mini-carrinho .header img').attr('src', clientesClass.cliente.foto);
        $('#page-produtos .mini-carrinho .header h3').html(clientesClass.cliente.nome);
        $('#page-produtos .mini-carrinho .header h4 > span').html(clientesClass.cliente.cartelas - carrinhoClass.carrinho.cartelas);

        var linha     = false;
        var prod_nome = false;
        var prod_img  = false;
        
        objC.find('tr').remove();
        
        carrinhoClass.carrinho.valor_total = 0;
        
        $.each(carrinhoClass.carrinho.itens, function(idx, item){
            var prod = produtosClass.findProduto(item.id_produto);
            
            carrinhoClass.carrinho.valor_total+= item.valor_total;
            
            linha = tpl;
            linha = linha.replace('##ID##',             item.id_produto);
            linha = linha.replace('##VENDA##',          item.id_venda_produto);
            linha = linha.replace('##IMG##',            prod.imagem);
            linha = linha.replace('##TITULO##',         prod.apelido);
            linha = linha.replace('##QTD##',            item.quantidade);
            linha = linha.replace('##VALOR_UNITARIO##', formataPreco(item.valor_unitario));

            objC.append(linha);
        });

        $('#page-produtos .mini-carrinho .footer .valor').html(formataPreco(carrinhoClass.carrinho.valor_total)); 
        
        prod_nome = null;
        prod_img  = null;
        linha     = null;
        objC      = null;
        tpl       = null;
        
        if(!$('#page-grade').is(":visible") && !$('#page-produtos .mini-carrinho').is(":visible") && ret === true) carrinhoClass.beginRenderCarrinhoFinal(false);
        
        carrinhoClass.processCarrinhoItensGrade(cb);
    },
    
    processCarrinhoItensGrade: function(cb){
        var arrItensGrade = {};

        execSQL("\
            SELECT  p.id_produto, \
	            c.nome AS categoria, \
                    p.apelido AS apelido, \
                    vp.id_parent, \
                    gc.visivel \
            FROM    produto AS p \
            LEFT JOIN    item_tabela_precos AS itp ON itp.id_produto = p.id_produto \
            LEFT JOIN    categoria AS c ON c.id_categoria = p.id_categoria \
            JOIN    venda_produto AS vp ON vp.id_produto = p.id_produto \
            LEFT JOIN grade_consumo AS gc ON gc.id_grade_consumo = vp.id_grade_consumo \
            WHERE   vp.id_venda = ? \
		AND vp.id_parent IS NOT NULL \
		AND p.visivel = 'true' \
                AND itp.id_uf = ? \
                AND vp.quantidade > 0 \
            GROUP BY    p.id_produto, c.id_categoria, vp.id_venda_produto \
            ORDER BY    p.id_categoria", [ carrinhoClass.carrinho.id_venda, localStorage.getItem('id_uf') ], function(tx, res) {

            for(var i = 0; i < res.rows.length; i++){
                if(typeof arrItensGrade[res.rows.item(i).id_parent] == "undefined") arrItensGrade[res.rows.item(i).id_parent] = [];
                
                arrItensGrade[res.rows.item(i).id_parent].push(res.rows.item(i));
            }
            
            $('.mini-carrinho .lista table tbody tr').each(function(){
                if(typeof arrItensGrade[$(this).attr('data-venda')] != "undefined"){
                    var linha = $(this);
                    var item = arrItensGrade[$(this).attr('data-venda')];
                    
                    $.each(item, function(idx, val){
                        if(val.visivel == 'true') linha.find('.grade').append('<span style="display:block; padding-left: 10px;">- ' + val.apelido + '</span>');
                    });
                }
            });
            
            if(typeof cb == "function") cb();
        });
        
    },
    
    beginRenderCarrinhoFinal: function(ret, callback){
        var objB = $('#page-carrinho .lista table tbody');
        var tpl  = $('#templates .tpl-carrinho-completo-item tbody').html();

        var id_cliente  = carrinhoClass.carrinho.id_cliente;
        var id_venda    = carrinhoClass.carrinho.id_venda;
        var id_uf       = localStorage.getItem('id_uf');

        var valor_unitario = 0;
        var valor_subtotal = 0;
        var valor_total    = 0;
        
        var sobra_cartela  = true;
        
        carrinhoClass.carrinho.cartelas = 0;
        
        carrinhoClass.carrinho.itensGrade = [];
        
        var sql = "\
            SELECT  p.id_produto, \
                    p.id_categoria, \
                    p.apelido AS apelido, \
                    p.visivel, \
                    pi.imagem, \
                    vp.id_venda_produto, \
                    vp.id_grade_consumo, \
                    vp.valor_venda, \
                    vp.quantidade, \
                    vp.id_parent, \
                    vp.id_forma_pagamento, \
                    (SELECT gc.id_grade_consumo FROM grade_consumo AS gc WHERE gc.id_produto = p.id_produto LIMIT 1) AS grade, \
                    (SELECT vp2.id_produto FROM venda_produto AS vp2 WHERE vp2.id_venda_produto = vp.id_parent LIMIT 1) AS id_produto_pai, \
                    gc.visivel AS grade_visivel, \
                    itp.aceita_cartela_digital, \
                    itp.preco_aberto, \
                    itp.credito_cartela, \
                    itp.venda, \n\
                    (CASE WHEN vp.id_parent IS NOT NULL THEN vp.id_parent ELSE vp.id_venda_produto END) AS agrupa \
            FROM    produto AS p \
            LEFT JOIN    produto_imagem AS pi ON pi.id_produto = p.id_produto \
            LEFT JOIN    item_tabela_precos AS itp ON itp.id_produto = p.id_produto \
            JOIN    venda_produto AS vp ON vp.id_produto = p.id_produto \
            LEFT JOIN   grade_consumo AS gc ON gc.id_grade_consumo = vp.id_grade_consumo \
            JOIN    venda AS v ON v.id_venda = vp.id_venda \
            WHERE   v.id_venda = ? \
                AND SUBSTR(v.data_fim, 1, 4) = '1970' \
                AND itp.id_uf = ? \
                AND vp.quantidade > 0 \
            GROUP BY vp.id_venda_produto \
            ORDER BY    agrupa ASC, vp.id_parent, p.id_categoria, vp.id_venda_produto";
        
        execSQL(sql, [ id_venda, id_uf ], function(tx, res) {

            objB.find('tr').remove();

            var tot_cartelas = 0;
            for(var i = 0; i < res.rows.length; i++){
                if(!res.rows.item(i).id_parent && res.rows.item(i).id_forma_pagamento == 9) tot_cartelas+= res.rows.item(i).quantidade;
            }
            sobra_cartela = tot_cartelas < clientesClass.cliente.cartelas;

            for(var i = 0; i < res.rows.length; i++){
                var linha = res.rows.item(i);
                
                var item = tpl;
                
                var val_qtd           = (linha.quantidade + "x");
                var val_class_add     = 'on';
                var val_class_del     = 'on';
                var val_class_fixo    = 'on';
                var val_class_aberto  = 'off';
                var val_cart_disabled = 'disabled="disabled"';
                var usa_cartela       = false;

                valor_unitario = 0;
                valor_subtotal = 0;

                if(!linha.id_parent){    // NAO É FILHO DE NENHUM OUTRO PRODUTO (ACESSO)
                    if(linha.aceita_cartela_digital == "true"){    // ACEITA CARTELA DIGITAL
                        if((clientesClass.cliente.cartelas - carrinhoClass.carrinho.cartelas) > 0){
                            val_cart_disabled = "";

                            if(linha.id_forma_pagamento == 9){
                                usa_cartela = true;
                                carrinhoClass.carrinho.cartelas+= linha.quantidade;
                            }
                        }

                    } else {    // NAO ACEITA CARTELA DIGITAL

                    }
                    
                    if(linha.preco_aberto == "true"){
                        val_class_fixo    = 'off';
                        val_class_aberto  = 'on';
                    }
                    
                    if(!usa_cartela) valor_unitario = (linha.valor_venda ? linha.valor_venda : linha.venda);
                    
                    valor_subtotal = linha.quantidade * valor_unitario;
                } else if(linha.grade_visivel == 'true'){    // FAZ PARTE DE UM GRUPO (ACESSO)
                    val_class_add = 'off';
                    val_class_del = 'off';
                    
                    if(typeof carrinhoClass.carrinho.itensGrade[linha.id_produto_pai] === "undefined") carrinhoClass.carrinho.itensGrade[linha.id_produto_pai] = [];
                    
                    carrinhoClass.carrinho.itensGrade[linha.id_produto_pai].push({
                        id_grade_consumo: linha.id_grade_consumo,
                        id_categoria:     linha.id_categoria,
                        id_produto:       linha.id_produto,
                        quantidade:       linha.quantidade1
                    });
                    
                    valor_subtotal = valor_unitario / linha.quantidade;
                }
                
                if(!linha.id_parent || linha.grade_visivel == 'true'){
                    if(!sobra_cartela && usa_cartela) val_class_add = 'off';

                    valor_total   += valor_subtotal;

                    carrinhoClass.carrinho.valor_total = valor_total;

                    item = item.replace('##ID##',               linha.id_produto);
                    item = item.replace('##ID_VENDA_PROD##',    linha.id_venda_produto);
                    item = item.replace('##GRADE##',            linha.grade);

                    item = item.replace('##IMG##',              linha.imagem);
                    item = item.replace('##TITULO##',           linha.apelido);

                    item = item.replace('##QTD##',              val_qtd);
                    item = item.replace('##CLASS_ADD##',        val_class_add);
                    item = item.replace('##CLASS_DEL##',        val_class_del);

                    item = item.replace('##CLASS_FIXO##',       val_class_fixo);
                    item = item.replace('##CLASS_ABERTO##',     val_class_aberto);

                    item = item.replace('##CLASS_FIXO##',       val_class_fixo);
                    item = item.replace('##CLASS_ABERTO##',     val_class_aberto);
                    item = item.replace('##PRECO_ABERTO##',     formataPreco(valor_unitario));
                    item = item.replace('##VALOR##',            valor_unitario ? ('R$ ' + formataPreco(valor_unitario)) : '');
                    item = item.replace('##SUB_TOTAL##',        valor_subtotal ? ('R$ ' + formataPreco(valor_subtotal)) : '');

                    item = item.replace('##cartela_disabled##', val_cart_disabled);
                    item = item.replace('##usa_cartela##',      usa_cartela ? "checked" : "");

                    if( val_cart_disabled == '' ){
                        item = item.replace('##CARTELA_IS_USED##', '');
                    }else{
                        item = item.replace('##CARTELA_IS_USED##', 'off');
                    }

                    if(linha.visivel == 'true') objB.append(item);
                }
            }
            
            $('#page-carrinho .header img').attr('src', clientesClass.cliente.foto);
            $('#page-carrinho .header h3').html(clientesClass.cliente.nome);
            $('#page-carrinho .header h4 > span').html(clientesClass.cliente.cartelas - carrinhoClass.carrinho.cartelas);
            $('#page-carrinho .footer .valor').html(formataPreco(valor_total)); 
            
            carrinhoClass.endRenderCarrinhoFinal();

            if( typeof callback == 'function' ) {
                callback();
            }
        });        
    },
    
    processUsaCartela: function(cb, id_produto_venda, usar, valor_venda, quantidade){
        if(usar && !clientesClass.cliente.cartelas) return false;
        
        var forma_pagto = null;
        
        if(usar){
            valor_venda = 0;
            forma_pagto = 9;
            
            carrinhoClass.carrinho.cartelas+= quantidade;

            if(debugDev) console.log('Marcando o produto com uso de cartela');
        } else {
            carrinhoClass.carrinho.cartelas-= quantidade;
            
            if(debugDev) console.log('Desmarcando o produto com uso de cartela');
        }
        
        execSQL("\
            UPDATE  venda_produto \
            SET valor_venda = ?, \
                id_forma_pagamento = ?, \
                dta = NULL \
            WHERE   id_venda_produto = ?", [ valor_venda, forma_pagto, id_produto_venda ], function(tx, res){

            execSQL("UPDATE controle SET atualizar = 1 WHERE tabela = 'venda_produto'", [ ], function(tx, res){
                syncClass.processPopulateTables();
                if(typeof cb === "function") cb("processUsaCartela");
            });
        });
    },
    
    endRenderCarrinhoFinal: function(){
        pagesClass.toPage("page-carrinho");
    },
    
    
    beginRenderPagamento: function(ret){
        var id_cliente  = carrinhoClass.carrinho.id_cliente;

        $('#page-pagamento .header img').attr('src', clientesClass.cliente.foto);
        $('#page-pagamento .header h3').html(clientesClass.cliente.nome);
        $('#page-pagamento .header h4 > span').html(clientesClass.cliente.cartelas - carrinhoClass.carrinho.cartelas);
        $('#page-pagamento .footer .valor').html(formataPreco(carrinhoClass.carrinho.valor_total)); 
        
        carrinhoClass.endRenderPagamento(false);
    },
	
    endRenderPagamento: function(ret){
        $('#page-pagamento .lista ul').html('');
        
        execSQL("\
            SELECT  id, descricao \
            FROM    forma_pagamento  \
            WHERE   id <> 9", [], function(tx, res) {

            for(i=0; i<res.rows.length; i++){
                $('#page-pagamento .lista ul').append('<li role="presentation"><a href="#forma-pagamento" class="text-uppercase" data-id="' + res.rows.item(i).id + '">' + res.rows.item(i).descricao + '</a></li>');
            }

            pagesClass.toPage("page-pagamento");
        });
    },

    beginFinalizaVenda: function(ret){
        var id_ponto_de_venda  = localStorage.getItem('id_ponto_de_venda');

        var id_forma_pagamento = carrinhoClass.carrinho.forma_pagto;
        var id_cliente         = carrinhoClass.carrinho.id_cliente;
        var id_venda           = carrinhoClass.carrinho.id_venda;
        var id_uf              = localStorage.getItem('id_uf');

        var agora = now();
        var cmds = [];

        if(ret === false){
            if(debugDev) console.log("Finalizando a venda: " + id_venda);
            
            var usou_cartela = false;
            var tot_estrelas = 0;
            
            var agora = now();
            
            var item = {};
            
            execSQL("\
                SELECT  p.id_produto, \
                        p.quantidade_estrelas, \
                        vp.id_venda_produto, \
                        vp.valor_venda, \
                        vp.quantidade, \
                        vp.id_parent, \
                        vp.id_forma_pagamento, \
                        itp.preco_aberto, \
                        itp.credito_cartela, \
                        itp.venda \
                FROM    produto AS p \
                JOIN    item_tabela_precos AS itp ON itp.id_produto = p.id_produto \
                JOIN    venda_produto AS vp ON vp.id_produto = p.id_produto \
                JOIN    venda AS v ON v.id_venda = vp.id_venda \
                WHERE   v.id_venda = ? \
                    AND SUBSTR(v.data_fim, 1, 4) = '1970' \
                    AND p.visivel = 'true' \
                    AND vp.id_parent IS NULL \
                    AND itp.id_uf = ? \
                    AND vp.quantidade > 0 \
                ORDER BY    vp.id_venda_produto", [ id_venda, id_uf ], function(tx, res) {

                for(var i = 0; i < res.rows.length; i++){
                    var linha = res.rows.item(i);
                    
                    item.id_venda_produto   = linha.id_venda_produto;
                    item.id_forma_pagamento = linha.id_forma_pagamento == 9 ? linha.id_forma_pagamento : id_forma_pagamento;
                    item.valor_venda        = id_forma_pagamento == 5 || linha.id_forma_pagamento == 9 ? 0 : linha.valor_venda;
                    item.preco_aberto       = linha.preco_aberto;
                    item.quantidade         = linha.quantidade;
                    
                    if(id_forma_pagamento != 5) tot_estrelas = linha.quantidade_estrelas > tot_estrelas ? linha.quantidade_estrelas : tot_estrelas;
                    
                    if(debugDev) console.log("Atualizando o produto: " + item.id_venda_produto);
                    
                    cmds.push(" UPDATE  venda_produto \
                                SET valor_venda = '" + item.valor_venda + "', \
                                    id_forma_pagamento = '" + item.id_forma_pagamento + "', \
                                    dta = NULL \
                                WHERE   id_venda_produto = '" + item.id_venda_produto + "'");

                    if(item.id_forma_pagamento == 9){
                        usou_cartela = true;
                        clientesClass.cliente.cartelas--;
                        
                        if(debugDev) console.log("Usando a cartela digital para o produto: " + item.id_venda_produto + " | qtd: " + item.quantidade);
                        
                        cmds.push(" UPDATE  cartela_digital \
                                    SET data_hora_utilizacao = '" + agora + "', \
                                        ativo = 'false', \
                                        dta = NULL \
                                    WHERE   id IN ( SELECT    id \
                                                    FROM  cartela_digital \
                                                    WHERE   id_cliente = '" + id_cliente + "' \
                                                        AND ativo = 'true' \
                                                        AND data_hora_utilizacao IS NULL \
                                                    LIMIT " + item.quantidade + ")");
                    }
                    
                    if(linha.credito_cartela == 'true'){
                        usou_cartela = true;
                        
                        if(debugDev) console.log("Incluindo cartela digital para o produto: " + item.id_venda_produto);
                        
                        for(i=0;i<item.quantidade;i++){
                            clientesClass.cliente.cartelas++;
                            
                            cmds.push(" INSERT INTO cartela_digital \
                                            (id_cliente, id_ponto_de_venda, data_hora_aquisicao, tipo, ativo, dta) \
                                        VALUES \
                                            ('" + id_cliente + "', '" + id_ponto_de_venda + "', '" + agora + "', 1, 'true', NULL)");
                        }
                    }
                }
                
                if(tot_estrelas){
                    if(debugDev) console.log("Inserindo as estrelas da venda - qtd: " + tot_estrelas);
                    
                    cmds.push("\
                                INSERT INTO estrelas \
                                    (id_cliente, id_ponto_de_venda, id_venda, id_origem_estrela, quantidade, dta) \
                                VALUES \
                                    ('" + id_cliente + "', '" + id_ponto_de_venda + "', '" + id_venda + "', 1, '" + tot_estrelas + "', NULL)");
                }
                    
                cmds.push("UPDATE controle SET atualizar = 1 WHERE tabela IN ('venda_produto'" + (usou_cartela ? ",'cartela_digital'" : "") + (tot_estrelas ? ",'estrelas'" : "") + ")");
                    
                execSQLBatch(0, cmds, function(){ carrinhoClass.beginFinalizaVenda("parte-1"); });                        
            });
        }
        
        if(ret === "parte-1"){
            execSQL("\
                SELECT COUNT(id_venda) AS vendas_mes \
                FROM venda \
                WHERE id_cliente = ? \
                AND STRFTIME('%m', data_fim) = STRFTIME('%m', ?)", [ id_cliente, agora ], function(tx, res) {

                var vendas_mes = res.rows.item(0).vendas_mes;

                if(vendas_mes == 8){
                    if(debugDev) console.log('Vendas mes = 8, inserindo mais 4 estrelas');

                    execSQL("\
                        INSERT INTO estrelas \
                            (id_cliente, id_ponto_de_venda, id_venda, id_origem_estrela, quantidade, dta) \
                        VALUES \
                            (?, ?, ?, ?, ?, ?)", [ id_cliente, id_ponto_de_venda, id_venda, 3, 4, null ], function(tx, res) {

                        execSQL("UPDATE controle SET atualizar = 1 WHERE tabela = 'estrelas'", [ ], function(tx, res) {
                            carrinhoClass.beginFinalizaVenda("parte-2");
                        });
                    });
                } else {
                    carrinhoClass.beginFinalizaVenda("parte-2");
                }
            });
        }
        
        if(ret === "parte-2"){
            if(debugDev) console.log('Atualizando e finalizando venda: ' + id_venda);

            execSQL("\
                UPDATE  venda \
                SET data_fim = ?, \
                    status = ?, \
                    dta = NULL \
                WHERE   id_venda = ?", [ agora, 1, id_venda ], function(tx, res) {

                execSQL("UPDATE controle SET atualizar = 1 WHERE tabela = 'venda'", [ ], function(tx, res) {
                    if(debugDev) console.log('Venda finalizada - limpando carrinho');
                    
                    carrinhoClass.resetCarrinho();
                    
                    syncClass.processPopulateTables();
                    
                    pagesClass.toPage("page-clientes-lista");
                });
            });            
        }
    },
    
    renderVendasAbertas: function(){
        var tpl  = $('#templates .tpl-vendas-item').html();

        execSQL("\
            SELECT  v.id_venda, COUNT(p.id_produto) AS quantidade_produtos, \
                    c.id_cliente, c.foto AS foto_cliente, c.nome AS nome_cliente \
            FROM    anfitriao AS a, cliente AS c, venda AS v \
            LEFT JOIN venda_produto AS vp ON vp.id_venda = v.id_venda AND vp.id_parent IS NULL AND vp.quantidade > 0 \
            LEFT JOIN produto AS p ON p.id_produto = vp.id_produto AND p.visivel = 'true' \
            WHERE   c.id_cliente = v.id_cliente \
                AND v.status = 0 \
                AND v.id_anfitriao = a.id_anfitriao \
                AND a.id_ponto_de_venda = ? \
            GROUP BY    v.id_venda, c.id_cliente \
            ORDER BY    v.data_inicio DESC", [ localStorage.getItem('id_ponto_de_venda') ], function(tx, results_inicial) {
            
            $('#page-vendas .lista').html("");
            
            if(results_inicial.rows.length > 0){
                for(var i = 0; i < results_inicial.rows.length; i++){
                    if(results_inicial.rows.item(i).quantidade_produtos){
                        var html = tpl;

                        var foto = results_inicial.rows.item(i).foto_cliente;

                        html = html.replace('##ID##',       results_inicial.rows.item(i).id_cliente);
                        html = html.replace('##PRODUTOS##', results_inicial.rows.item(i).quantidade_produtos);
                        html = html.replace('##FOTO##',     foto);
                        html = html.replace('##NOME##',     results_inicial.rows.item(i).nome_cliente);

                        $('#page-vendas .lista').append(html);
                    }
                }
                
            }
            
            pagesClass.toPage("page-vendas");
        });
    }
    
};