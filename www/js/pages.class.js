window.pageActual = "page-login";
window.pageLast   = false;

var pagesClass = {
    pageTitles: { 
        "page-login":               "Login",
        "page-clientes-lista":      "Clientes", 
        "page-clientes-incluir":    "Incluir Cliente", 
        "page-clientes-editar":     "Editar Cliente", 
        "page-vendas":              "Vendas Ativas",
        "page-produtos":            "Produtos",
        "page-carrinho":            "Carrinho de Compras",
        "page-grade":               "Escolha de Produtos",
        "page-pagamento":           "Forma de Pagamento",
        "page-relatorio":           "Relatórios",
        "page-debug":               "Debug"
    },
    
    toPage: function(pageNew, customLeavingCallback, customLoadingCallback){
        var pageOld = $('.page.active').attr('id');
        
        if(pageNew == "page-login" && $.trim(localStorage.getItem('login')) && $.trim(localStorage.getItem('senha'))){
            authClass.processLogin(localStorage.getItem('login'), localStorage.getItem('senha'));
            return false;
        }
        
        if(pageOld === pageNew) return false;

        window.pageLast   = pageOld;
        window.pageActual = pageNew;
        
        if(!window.isLogged && pageNew !== "page-login" && pageNew !== "page-debug"){
            pagesClass.toPage("page-login");
            return false;
        }
        
        if(typeof customLeavingCallback === "function"){
            customLeavingCallback(pageOld, pagesClass.leavingPage);
        } else {
            pagesClass.leavingPage(pageOld);
        }

        if(typeof customLoadingCallback === "function"){
            customLoadingCallback(pageNew, pagesClass.loadingPage);
        } else {
            pagesClass.loadingPage(pageNew);
        }

        pagesClass.pageHistory.push( pageNew );
    },

    leavingPage: function(page){
        $('.page').hide('fast').removeClass('active');
    },

    loadingPage: function(page){
        pagesClass.preparePage(page);
        $('.navbar .navbar-text').html(pagesClass.pageTitles[page]);
        $('.page#' + page).show('fast').addClass('active');
    },
    
    preparePage: function(page){
        $('.navbar .controles').hide();
        $('.navbar .controles a').hide();
        
        switch(page){
            case 'page-clientes-lista':
            localStorage.removeItem('id_cliente');
            clientesClass.resetCliente();

            $('.navbar .controles').show();
            $('.navbar .controles a#btn-add-cliente').show();
            $('.navbar .controles a#btn-lista-vendas').show();
            break;
            
            case 'page-clientes-editar':
            case 'page-clientes-incluir':
            $('.navbar .controles').show();
            $('.navbar .controles a#btn-back-cliente').show();
            $('.navbar .controles a#btn-lista-vendas').show();
            $('#page-clientes-editar .nav-tabs a[href="#perfil"]').tab('show');
            break;
            
            case 'page-vendas':
            $('.navbar .controles').show();
            $('.navbar .controles a#btn-back-cliente').show();
            break;

            case 'page-produtos':
            $('.navbar .controles').show();
            $('.navbar .controles a#btn-go-carrinho').show();
            $('.navbar .controles a#btn-lista-vendas').show();
            break;

            case 'page-carrinho':
            case 'page-grade':
            $('#page-carrinho .lista .alert').hide();
            $('.navbar .controles').show();
            $('.navbar .controles a#btn-back-produtos').show();
            $('.navbar .controles a#btn-lista-vendas').show();
            break;

            case 'page-pagamento':
            $('#page-pagamento .lista .alert').hide();
            $('.navbar .controles').show();
            $('.navbar .controles a#btn-back-carrinho').show();
            break;

            case 'page-relatorio':
            $('.navbar .controles').show();
            $('.navbar .controles a#btn-back-cliente').show();
            break;
            
            case 'relatorio-venda-diario':
            case 'relatorio-atividade-diario':
            case 'relatorio-mensal-ganho':
            case 'relatorio-anfitrioes':
            pagesClass.renderRelatorio(page);
            break;
        }
    },

    renderRelatorio: function(relatorio){
        var arrReportsUrls = { 
            'relatorio-venda-diario':     urlReport + '/relatorio-detalhado-vendas-diario', 
            'relatorio-atividade-diario': urlReport + '/relatorio-detalhado-atividades-diario',
            'relatorio-mensal-ganho':     urlReport + '/relatorio-mensal-ganho', 
            'relatorio-anfitrioes':       urlReport + '/anfitrioes-evs'
        };

        $('#page-relatorio #frame-relatorio').attr('src', arrReportsUrls[relatorio]);

        pagesClass.toPage('page-relatorio');
    },

    requestExit: false,
    pageHistory: [],

    goBack: function(){
        if( !pagesClass.requestExit ) {
            if( pagesClass.pageHistory.length > 1 && pagesClass.pageHistory[pagesClass.pageHistory.length - 1] != 'page-clientes-lista' && pagesClass.pageHistory[pagesClass.pageHistory.length - 1] != 'page-login' ) {
                pagesClass.leavingPage( pagesClass.pageHistory[pagesClass.pageHistory.length - 1] );
                pagesClass.pageHistory.length--;
                pagesClass.loadingPage( pagesClass.pageHistory[pagesClass.pageHistory.length - 1] );
            } else {
                if( typeof cordova === 'undefined' ){
                    console.log('impossível sair do app');
                }else{
                    window.plugins.toast.showWithOptions({
                        message: 'Aperte novamente para sair.',
                        duration: 3000,
                        position: 'bottom'
                    }, function() {
                        pagesClass.requestExit = true;
                        
                        var exit = setTimeout(function(){
                            pagesClass.requestExit = false;
                            exit = null;
                        }, 3000);
                    });
                }
            }
        }else{
            navigator.app.exitApp();
        }
    }
};
