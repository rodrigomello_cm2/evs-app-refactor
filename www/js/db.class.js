/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var db = false;


function initDB() {
    try {
        db = window.openDatabase(dbName, "1.0", dbDisplayName, 1024 * 19500);
        if(debugDev) console.log("Banco de dados inicializado");
    } catch (e) {
        if(debugDev) console.log("Erro ao inicializar banco de dados");
    }
}

function execSQL(cmd, pars, funcao, funcaoErro) {
    if(typeof funcaoErro !== "function"){
        funcaoErro = function(t, e){
            if(debugDev) console.log("Erro ao executar SQL: " + e.code + ' / ' + e.message);
        };
    }
    
    db.transaction(function (tx) {
        try {
            tx.executeSql(cmd, pars, funcao, funcaoErro);
        } catch (e) {
            if(debugDev) console.log("Erro de execucao statement SQL: " + e);
        }
    }, dbErro);
}

function execSQLBatch(i, cmds, callbackBatch){
    if(typeof callbackBatch !== "function"){
        callbackBatch = function(){
            // nothing to do
        };
    }

    if(cmds.length > i){
        var sql   = cmds[i];
        var param = [];
        
        if(typeof sql === "object"){
            sql   = cmds[i].sql;
            param = cmds[i].values;
        }
        
        execSQL(sql, param, function() {
            execSQLBatch(i + 1, cmds, callbackBatch);
        });
    } else {
        callbackBatch();
    }
}

function dbErro(trx, err) {
    if(debugDev) console.log("Erro de transacao SQL:");
    if(debugDev) console.log(trx);
}
                                                                                                                                                                                                                                                                            