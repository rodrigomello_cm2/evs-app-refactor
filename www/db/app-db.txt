CREATE TABLE IF NOT EXISTS controle
(
    tabela TEXT NOT NULL PRIMARY KEY,
    atualizar INTEGER NOT NULL,
    dta    TEXT NOT NULL
)
;
CREATE TABLE IF NOT EXISTS venda_produto
(
    id_venda_produto 		text NOT NULL PRIMARY KEY,
    id_produto       		integer NOT NULL,
    id_venda         		text NOT NULL,
    id_forma_pagamento		integer,
    quantidade       		float NOT NULL,
    valor_venda      		float,
    valor_custo      		float,
    id_parent        		text,
    id_grade_consumo 		integer,
    quantidade1      		float,
    valor_custo1      		float,
    pontos_de_volume      	float,
    dta              		text
)
;
CREATE TABLE IF NOT EXISTS produto
(
    id_produto   			integer NOT NULL PRIMARY KEY,
    id_categoria 			integer NOT NULL,
    nome         			text    NOT NULL,
    ean          			text    NULL,
    skuint          	    text    NULL,
    sku          			text    NULL,
    descricao    			text    NOT NULL,
    visivel      			boolean    NOT NULL,
    apelido      			text,
    quantidade_estrelas     integer,
    dta          			text
)
;
CREATE TABLE IF NOT EXISTS cliente
(
    id_cliente   	text NOT NULL PRIMARY KEY,
    id_anfitriao 	integer NOT NULL,
    id_tipo_indicacao 	integer,
    id_cliente_indicou 	text,
    nome         	text NOT NULL,
    email               text NOT NULL,
    foto         	text NOT NULL,
    data_hora_inclusao  text,
    telefone_celular    text,
    data_nascimento     text,
    sexo                text,
    senha               text,
    dta          	text
)
;
CREATE TABLE IF NOT EXISTS cliente_bioimpedancia
(
    id			integer NOT NULL PRIMARY KEY,
    id_cliente   	text 	NOT NULL,
    data_hora		text	NOT NULL,
    imc      		text,
    perc_gordura        text,
    perc_musculo        text,
    metabolismo         text,
    gordura_visceral    float,
    idade_corporal      text,
    token		text,
    ativo               boolean,
    dta			text
)
;
CREATE TABLE IF NOT EXISTS cliente_medidas
(
    id			integer NOT NULL PRIMARY KEY,
    id_cliente   	text 	NOT NULL,
    data_hora		text	NOT NULL,
    peso      		text,
    altura     		text,
    torax               text,
    cintura             text,
    barriga             text,
    quadril             text,
    token		text,
    ativo               boolean,
    dta			text
)
;
CREATE TABLE IF NOT EXISTS venda
(
    id_venda      text NOT NULL PRIMARY KEY,
    id_cliente    text NOT NULL,
    id_anfitriao  integer,
    data_inicio   text    NOT NULL,
    data_fim      text    NOT NULL,
    status	  integer,
    dta           text
)
;
CREATE TABLE IF NOT EXISTS tag_produto
(
    id_tag_produto integer NOT NULL PRIMARY KEY,
    id_produto     integer NOT NULL,
    id_tag         integer NOT NULL,
    dta            text
)
;
CREATE TABLE IF NOT EXISTS tag
(
    id_tag 			integer		NOT NULL PRIMARY KEY,
    nome   			text    	NOT NULL,
    id_parent	   		integer NULL,
    visivel		   	text,
    exibir_auto_produtos	text,
    exibir_categoria		text,
    dta    			text
)
;
CREATE TABLE IF NOT EXISTS classe_ponto_de_venda
(
    id_classe_ponto_de_venda integer NOT NULL PRIMARY KEY,
    descricao                text    NOT NULL,
    dta                      text
)
;
CREATE TABLE IF NOT EXISTS historico_padrao_custos
(
    id		   integer NOT NULL PRIMARY KEY,
    descricao      text    NOT NULL,
    dta            text
)
;
CREATE TABLE IF NOT EXISTS custos_mensais
(
    id		   integer NOT NULL PRIMARY KEY,
    id_historico   integer NOT NULL,
    valor	   float,
    dta            text
)
;
CREATE TABLE IF NOT EXISTS ponto_de_venda
(
    id_ponto_de_venda        integer NOT NULL PRIMARY KEY,
    id_classe_ponto_de_venda integer NOT NULL,
    id_uf                    integer NOT NULL,
    id_anfitriao             integer,
    id_anfitriao_master      integer,
    nome                     text    NOT NULL,
    endereco                 text    NOT NULL,
    telefone                 text    NOT NULL,
    site                     text    NOT NULL,
    data_expiracao           text,
    dta                      text
)
;
CREATE TABLE IF NOT EXISTS anfitriao
(
    id_anfitriao          integer NOT NULL PRIMARY KEY,
    id_ponto_de_venda     integer NOT NULL,
    username              text    NOT NULL,
    username_canonical    text    NOT NULL,
    email                 text    NOT NULL,
    email_canonical       text    NOT NULL,
    enabled               text    NOT NULL,
    salt                  text    NOT NULL,
    password              text    NOT NULL,
    last_login            text    NULL,
    locked                text    NOT NULL,
    expired               text    NOT NULL,
    expires_at            text    NULL,
    confirmation_token    text    NULL,
    password_requested_at text    NULL,
    roles                 text    NOT NULL,
    credentials_expired   text    NOT NULL,
    credentials_expire_at text    NULL,
    nome                  text    NOT NULL,
    endereco              text    NOT NULL,
    faixa		  integer NOT NULL,
    telefone              text,
    cpf              	  text,
    dta                   text
)
;
CREATE TABLE IF NOT EXISTS grade_consumo
(
    id_grade_consumo      integer NOT NULL PRIMARY KEY,
    id_categoria          integer,
    id_produto            integer NOT NULL,
    quantidade            float   NOT NULL,
    id_produto_especifico bigint,
    descricao		  text,
    visivel		  text,
    dta                   text
)
;
CREATE TABLE IF NOT EXISTS grade_consumo_pdv
(
    id_grade_consumo_pdv  integer NOT NULL PRIMARY KEY,
    id_ponto_de_venda     integer NOT NULL,
    id_grade_consumo      integer NOT NULL,
    quantidade            float   NOT NULL,
    ativo                 boolean NOT NULL,
    dta                   text
)
;
CREATE TABLE IF NOT EXISTS uf
(
    id_uf integer NOT NULL PRIMARY KEY,
    nome  text    NOT NULL,
    dta   text
)
;
CREATE TABLE IF NOT EXISTS item_tabela_precos
(
    id_item_tabela_precos 	integer NOT NULL PRIMARY KEY,
    id_produto            	integer NOT NULL,
    id_tabela_precos      	integer NOT NULL,
    id_uf                 	integer NOT NULL,
    ponto_valor           	float,
    venda                 	float,
    custo25               	float,
    custo35               	float,
    custo42               	float,
    custo50               	float,
    aceita_cartela_digital      boolean,
    preco_aberto		boolean,
    credito_cartela		boolean,
    dta                  	text
)
;
CREATE TABLE IF NOT EXISTS categoria
(
    id_categoria     integer NOT NULL PRIMARY KEY,
    id_categoria_pai integer NULL,
    nome             text    NOT NULL,
    descricao        text    NOT NULL,
    dta              text
)
;
CREATE TABLE IF NOT EXISTS tabela_precos
(
    id_tabela_precos         integer NOT NULL PRIMARY KEY,
    id_classe_ponto_de_venda integer NOT NULL,
    data_inicio              text    NOT NULL,
    data_fim                 text    NULL,
    descricao                text    NOT NULL,
    dta                      text
)
;
CREATE TABLE IF NOT EXISTS cartela_digital
(
    id		    	   integer NOT NULL PRIMARY KEY,
    id_cliente  	   text    NOT NULL,
    id_ponto_de_venda      integer NOT NULL,
    data_hora_aquisicao    text    NOT NULL,
    data_hora_utilizacao   text,
    tipo		   integer NOT NULL,
    ativo		   boolean NOT NULL,
    dta        		   text
)
;
CREATE TABLE IF NOT EXISTS forma_pagamento
(
    id	   		integer NOT NULL PRIMARY KEY,
    descricao           text    NOT NULL,
    dta    		text
)
;
CREATE TABLE IF NOT EXISTS tipo_indicacao
(
    id	   		integer NOT NULL PRIMARY KEY,
    descricao           text    NOT NULL,
    dta    		text
)
;
CREATE TABLE IF NOT EXISTS origem_estrela
(
    id	   		integer NOT NULL PRIMARY KEY,
    descricao           text    NOT NULL,
    dta    		text
)
;
CREATE TABLE IF NOT EXISTS estrelas
(
    id		    	   integer NOT NULL PRIMARY KEY,
    id_cliente  	   text    NOT NULL,
    id_ponto_de_venda      integer NOT NULL,
    id_venda  	       	   text,
    id_origem_estrela      integer NOT NULL,
    quantidade		   float,
    dta        		   text
)
;
CREATE TABLE IF NOT EXISTS item_pdv_tabela_precos
(
    id		    	       integer NOT NULL PRIMARY KEY,
    id_produto  	       integer NOT NULL,
    id_ponto_de_venda          integer NOT NULL,
    preco		       float,
    ativo                      boolean NOT NULL,
    dta        		       text
)
;
CREATE TABLE IF NOT EXISTS cliente_foto
(
    id 	  		integer NOT NULL PRIMARY KEY,
    id_cliente          integer NOT NULL,
    data_hora           text	NOT NULL,
    imagem 		text,
    token		text,
    ativo               boolean,
    dta 		text
)
;
CREATE TABLE IF NOT EXISTS produto_imagem
(
    id 	  		integer NOT NULL PRIMARY KEY,
    id_produto          integer NULL,
    imagem 		text,
    dta 		text
)
